-- Don't insert value for AUTO_INCREMENT when value is `0`
-- See: https://mariadb.com/kb/en/sql-mode/#no_auto_value_on_zero
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- Set Time Zone for WIB (GMT+7)
SET time_zone = "+07:00";
