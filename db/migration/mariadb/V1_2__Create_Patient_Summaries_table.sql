CREATE TABLE `patient_summaries` (
  `id`         int(11)  NOT NULL AUTO_INCREMENT,
  `confirmed`  int(11)  NOT NULL,
  `recovered`  int(11)  NOT NULL,
  `deceased`   int(11)  NOT NULL,
  `timestamp`  datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
  DEFAULT CHARSET=utf8mb4
  COLLATE=utf8mb4_unicode_ci
  AUTO_INCREMENT=1;
