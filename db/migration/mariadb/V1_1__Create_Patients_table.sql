CREATE TABLE `patients` (
  `id`                   int(11)      NOT NULL AUTO_INCREMENT,
  `case_id`              varchar(200) NOT NULL,
  `age`                  int(11)      DEFAULT NULL,
  `nationality`          varchar(50)  DEFAULT NULL,
  `gender`               varchar(10)  DEFAULT NULL,
  `province`             varchar(200) DEFAULT NULL,
  `city`                 varchar(200) DEFAULT NULL,
  `first_contact_at`     datetime     DEFAULT NULL,
  `first_symptoms_at`    datetime     DEFAULT NULL,
  `first_isolation_at`   datetime     DEFAULT NULL,
  `declared_positive_at` datetime     DEFAULT NULL,
  `recovered_at`         datetime     DEFAULT NULL,
  `deceased_at`          datetime     DEFAULT NULL,
  `hospitalized_in`      text         DEFAULT NULL,
  `location_type`        varchar(100) DEFAULT NULL,
  `contact_reference`    varchar(200) DEFAULT NULL,
  `incubation_period`    int(11)      DEFAULT NULL,
  `notes`                text         DEFAULT NULL,
  `final_status`         varchar(50)  DEFAULT NULL,
  `created_at`           datetime     NOT NULL,
  `cluster`              varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
  DEFAULT CHARSET=utf8mb4
  COLLATE=utf8mb4_unicode_ci
  AUTO_INCREMENT=1;
