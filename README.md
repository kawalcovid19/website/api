# kawalcovid19-api

`kawalcovid19-api` is the provides an API web service for Patient cases list / history, and Case Summary Google sheet
data scraper (which is maintained by the content team) for [kawalcovid19.id].

## API Endpoints

### Data

  - `GET /api/case/summary`: Provides latest case number of COVID-19 in Indonesia
  - `GET /api/content/posts`: Provide (cached) access to Kawal COVID-19 WordPress posts
  - `GET /proxy/{key}/**`: Simple caching proxy to internal resources, see [Proxy Configuration](#proxy-configuration)

### System

  - `GET /api-docs/swagger.[json|yaml]`: OpenAPI (Swagger) specification describing operations and format of each
    endpoints
  - `GET /health`: Health Check endpoint to enable periodic testing for a service's instance availability

Note: All endpoints prefixed with `/api` _must have_ Swagger specification  

## Proxy Configuration

_Note: Currently only `GET` request with text-based payload are supported._

To configure proxy and routing, start the application with the `PROXY_ROUTES=?` environment variable:

  - Use `|` to separate entries, and
  - Use `=` to separate path segment and destination host

For example, with `PROXY_ROUTES="google=https://google.com | ddg=https://duckduckgo.com"` will route `GET` requests:

  - From `/api/content/google/search?q=hello` to `https://google.com/search?q=hello`
  - From `/api/content/ddg/search?q=hello` to `https://duckduckgo.com/search?q=hello`
  - From `/api/content/invalid/search?q=hello` will return 404 not found

## Prerequisites

The following tools are required to work with this project:

  - [Java SDK] 8+
  - [SBT] 1.3
  - [MariaDB] 10
  - [Docker] and docker-compose (Optional)

If you are using Linux or macOS, [sdkman] is a useful tool to install all the requirements above.

## Setup Steps

The application can be built and run via different methods:

  - SBT (shell) for development, and
  - All-in-one JAR for distribution 

Depending on how the application is run,

  - Start the web API server and access the application via web browser on [`localhost:12010`](http://localhost:12010),
    or
  - Start the scraping process and observe the result in database

The web API server can be configured to run the scraping process as a scheduled job in local development. Simply update
the value `FEATURE_JOB_ENABLE` to `true` in `.env`.

### Database Setup

To setup the database dependency, either use or install MariaDB locally or via [Docker] and docker-compose by running
the following command:

  - `docker-compose up -d`, or
  - `docker-compose up db -d` to specify the service explicitly

To create or update schema to the latest version, ensure the database is running and accessible, and run the following
command:

  - `sbt flywayMigrate`

### Development Mode

  1. Run `sbt` from command line (cmd, shell, bash, etc.) to enter SBT shell
  2. Run `reStart` to start the web API server in a forked process
  3. Run `~reStart` to start the web API server in a forked process and enable continuous build (watch for changes)
  4. Run `reStop` to stop the running web API server

Alternatively, invoking `sbt run` from command line will prompt several choices for main class runner in this project:

  - `id.kawalcovid19.api.server.Server` - runs the web API server at port 12010
  - `id.kawalcovid19.api.server.Scraper` - runs the Google sheet scraper

Press `ctrl+C` to exit the runner, and enter `exit` to exit the SBT shell.

## All-in-One JAR

For production purposes, this application is configured to produce an all-in-one web API JAR.

  - Run `assembly` to build the web API server JAR

After the build process is done, you can run the JAR file using:

  - `java -jar target/scala-2.12/*-assembly-*.jar` to run the web API server. Now you can visit it on
    [`localhost:12010`](http://localhost:12010) from your browser.


### Source Code Formatting

We use [scalafmt] for code-formatting. Ensure to setup your IDE to apply provided `.scalafmt.conf` rules, or 
alternatively:

  - Run `sbt scalafmt` to apply formatting to the codebase
  - Run `sbt scalafmtSbt` to apply formatting to the SBT configuration files (`*.sbt`)
  - Run `sbt scalafmtAll` to apply formatting to all Scala files (incl. SBT configuration files)

Currently [scalafmt] does not provide optimise imports capability, so ensure the following import order is set within
your IDE of choice for consistency:

  1. `java`
  2. `javax`
  3. `scala`
  4. _blank line / separator_
  5. _all other packages_
  6. _blank line / separator_
  7. `id.kawalcovid19`

## Contributing

We follow the "[feature-branch]" Git workflow.

  1. Commit changes to a branch in your fork (use `snake_case` convention):
     - For technical chores, use `chore/` prefix followed by the short description, e.g. `chore/do_this_chore`
     - For new features, use `feature/` prefix followed by the feature name, e.g. `feature/feature_name`
     - For bug fixes, use `bug/` prefix followed by the short description, e.g. `bug/fix_this_bug`
  2. Rebase or merge from "upstream"
  3. Submit a PR "upstream" to `develop` branch with your changes

Please read [CONTRIBUTING] for more details.

## License

```
  MIT License

  Copyright (c) 2020 kawalcovid19.id contributors

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
```

`kawalcovid19-api` is released under the MIT License. See the [LICENSE] file for further details.

[CONTRIBUTING]: https://gitlab.com/kawalcovid19/kawalcovid19-case-list-api/-/blob/master/CONTRIBUTING.md
[Docker]: https://www.docker.com/
[feature-branch]: http://nvie.com/posts/a-successful-git-branching-model/
[Java SDK]: https://adoptopenjdk.net/
[kawalcovid19.id]: https://kawalcovid19.id/
[LICENSE]: https://gitlab.com/kawalcovid19/kawalcovid19-case-list-api/-/blob/master/LICENSE.txt
[MariaDB]: https://mariadb.org/
[SBT]: https://www.scala-sbt.org/
[scalafmt]: https://scalameta.org/scalafmt/
[sdkman]: https://sdkman.io/
