import java.io.Closeable
import scala.io.Source

lazy val versionFile = "VERSION.txt"
lazy val appVersion = {
  def using[T <: Closeable, U](res: T)(fn: T => U): U =
    try fn(res)
    finally res.close()

  using(Source.fromFile(versionFile))(_.mkString)
}

organization := "id.kawalcovid19"
name := "api"
version := appVersion
scalaVersion := "2.12.11"

// Application dependencies
// ~~~~~~
resolvers += ("SqlLayer" at "http://nexus.nolimit.id/repository/sql-layer/")
  .withAllowInsecureProtocol(allowInsecureProtocol = true)

val akkaHttpVersion         = "10.1.11"
val akkaHttpCorsVersion     = "0.4.2"
val akkaHttpSwaggerVersion  = "2.0.0"
val akkaHttpPlayJsonVersion = "1.29.1"
val akkaHttpCirceVersion    = "1.29.1"
val akkaQuartzVersion       = "1.8.1-akka-2.5.x"
val akkaStreamKafkaVersion  = "2.0.2"
val elastic4sVersion        = "6.2.11"
val googleAuthVersion       = "0.16.2"
val jansiVersion            = "1.18"
val jasyptVersion           = "1.9.3"
val javaxWsRsVersion        = "2.0.1"
val logbackVersion          = "1.2.3"
val protobufJavaVersion     = "3.7.1"
val pureconfigVersion       = "0.11.0"
val scalacacheVersion       = "0.28.0"
val scalatestVersion        = "3.1.1"
val scallopVersion          = "3.4.0"
val sentryVersion           = "1.7.30"
val sqlLayerVersion         = "2.0.3"
val sttpVersion             = "2.0.6"
val mockitoVersion          = "3.3.3"

// Transient dependency versions
val akkaVersion     = "2.5.26" // From `akka-http`
val circeVersion    = "0.12.1" // From `akka-http-circe`
val playJsonVersion = "2.7.4"  // From `akka-http-play-json`

libraryDependencies ++= Seq(
  "ch.megard"                    %% "akka-http-cors"                 % akkaHttpCorsVersion,
  "ch.qos.logback"               % "logback-classic"                 % logbackVersion,
  "com.enragedginger"            %% "akka-quartz-scheduler"          % akkaQuartzVersion,
  "com.github.cb372"             %% "scalacache-caffeine"            % scalacacheVersion,
  "com.github.pureconfig"        %% "pureconfig"                     % pureconfigVersion,
  "com.github.swagger-akka-http" %% "swagger-akka-http"              % akkaHttpSwaggerVersion,
  "com.google.auth"              % "google-auth-library-oauth2-http" % googleAuthVersion,
  "com.google.protobuf"          % "protobuf-java"                   % protobufJavaVersion,
  "com.softwaremill.sttp.client" %% "akka-http-backend"              % sttpVersion,
  "com.softwaremill.sttp.client" %% "circe"                          % sttpVersion,
  "com.typesafe.akka"            %% "akka-actor"                     % akkaVersion,
  "com.typesafe.akka"            %% "akka-cluster"                   % akkaVersion,
  "com.typesafe.akka"            %% "akka-http"                      % akkaHttpVersion,
  "com.typesafe.akka"            %% "akka-http-testkit"              % akkaHttpVersion % Test,
  "com.typesafe.akka"            %% "akka-stream"                    % akkaVersion,
  "com.typesafe.akka"            %% "akka-stream-testkit"            % akkaVersion % Test,
  "com.typesafe.akka"            %% "akka-stream-kafka"              % akkaStreamKafkaVersion,
  "com.typesafe.akka"            %% "akka-testkit"                   % akkaVersion % Test,
  "com.typesafe.play"            %% "play-json"                      % playJsonVersion,
  "com.typesafe.akka"            %% "akka-slf4j"                     % akkaVersion,
  "de.heikoseeberger"            %% "akka-http-play-json"            % akkaHttpPlayJsonVersion,
  "de.heikoseeberger"            %% "akka-http-circe"                % akkaHttpCirceVersion,
  "id.nolimit"                   %% "sqllayer"                       % sqlLayerVersion,
  "io.circe"                     %% "circe-core"                     % circeVersion,
  "io.circe"                     %% "circe-generic"                  % circeVersion,
  "io.circe"                     %% "circe-parser"                   % circeVersion,
  "io.sentry"                    % "sentry-logback"                  % sentryVersion,
  "javax.ws.rs"                  % "javax.ws.rs-api"                 % javaxWsRsVersion,
  "org.fusesource.jansi"         % "jansi"                           % jansiVersion,
  "org.jasypt"                   % "jasypt"                          % jasyptVersion,
  "org.rogach"                   %% "scallop"                        % scallopVersion,
  "org.scalatest"                %% "scalatest"                      % scalatestVersion,
  "org.mockito"                  % "mockito-core"                    % mockitoVersion % Test
)

// Flyway schema migration configuration
// ~~~~~~
enablePlugins(FlywayPlugin)
flywayLocations += "filesystem:db/migration/mariadb"
flywayUrl := "jdbc:mysql://localhost:3306/kawalcovid19"
flywayUser := "dbuser"
flywayPassword := "P@ssw0rd"
// Test configuration is disabled until DB tests are written
//flywayUrl in Test := "jdbc:mysql://localhost:3306/kawalcovid19"
//flywayUser in Test := "dbuser"
//flywayPassword in Test := "P@ssw0rd"

// .env support in test
// ~~~~~~
envFileName in Test := ".env"
envVars in Test := (envFromFile in Test).value

// Copy `VERSION.txt` file in project root to be bundled with the application distributable
// See: https://stackoverflow.com/a/57994298
// ~~~~~~
val copyVersionFile = taskKey[Unit]("Copy VERSION.txt file to resource folder to embed version number")

copyVersionFile := {
  val srcFile     = (baseDirectory in Compile).value / versionFile
  val srcToTarget = srcFile pair Path.rebase(srcFile, (crossTarget in Compile).value / "classes" / versionFile)

  IO.copy(srcToTarget, CopyOptions.apply(overwrite = true, preserveLastModified = true, preserveExecutable = false))
}

compile := ((compile in Compile) dependsOn copyVersionFile).value
reStart := (reStart dependsOn copyVersionFile).evaluated
run := ((run in Compile) dependsOn copyVersionFile).evaluated

// App distribution packaging
// ~~~~~~
enablePlugins(JavaAppPackaging)
packageName in Universal := s"id.kawalcovid19.api-${version.value}"

// Default main class in all-in-one JAR distribution and `reStart`
// ~~~~~~
val serverMainClassName = "id.kawalcovid19.api.server.Server"
mainClass in reStart := Some(serverMainClassName)
