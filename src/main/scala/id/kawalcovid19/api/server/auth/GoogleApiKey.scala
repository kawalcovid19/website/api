package id.kawalcovid19.api.server.auth

final case class GoogleApiKey(value: String) extends AnyVal
