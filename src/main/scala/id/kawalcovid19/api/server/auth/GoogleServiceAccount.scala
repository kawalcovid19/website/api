package id.kawalcovid19.api.server.auth

import scala.io.Source

import org.jasypt.util.text.{AES256TextEncryptor, TextEncryptor}

import id.kawalcovid19.api.server.config.GoogleConfig
import id.kawalcovid19.api.server.helper.AutoCloseableSupport

final case class GoogleServiceAccount(credentials: String)

object GoogleServiceAccount extends AutoCloseableSupport {

  private final val BUNDLED_SERVICE_ACCOUNT_FILE =
    "credentials/kawalcovid19-api-backend-9d42a0475540.json.enc"

  /** Read Google Service Account credentials from application's Google config.
    *
    * It will prioritise reading external Service Account file, if provided, then uses bundled (encrypted) Service
    * Account file if decryption secret is provided.
    *
    * @param config Google configuration.
    * @return Google Service Account credentials.
    */
  def fromConfig(config: GoogleConfig): Option[GoogleServiceAccount] =
    (config.serviceAccountFile, config.serviceAccountSecret) match {
      case (Some(filePath), _) if filePath.trim.nonEmpty =>
        val value = using(Source.fromFile(filePath))(_.mkString)
        Option(new GoogleServiceAccount(value))

      case (_, Some(secret)) if secret.trim.nonEmpty =>
        val raw = using(Source.fromResource(BUNDLED_SERVICE_ACCOUNT_FILE))(_.mkString)
        val enc = encryptor(secret)
        Option(new GoogleServiceAccount(enc.decrypt(raw)))

      case _ =>
        Option.empty[GoogleServiceAccount]
    }

  private[this] def encryptor(password: String): TextEncryptor = {
    val enc = new AES256TextEncryptor
    enc.setPassword(password)
    enc
  }
}
