package id.kawalcovid19.api.server.config

/** Google Auth configuration.
  *
  * Maps to `google.*` entry in configuration.
  *
  * @param apiKey Google API key.
  * @param serviceAccountSecret Secret to decrypt bundled Google service account credentials file.
  * @param serviceAccountFile Path to (external) Google service account credentials file.
  */
final case class GoogleConfig(
    apiKey: String,
    serviceAccountSecret: Option[String],
    serviceAccountFile: Option[String]
)
