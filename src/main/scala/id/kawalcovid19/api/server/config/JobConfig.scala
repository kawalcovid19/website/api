package id.kawalcovid19.api.server.config

/** Scheduled Job Configuration.
  *
  * Maps to `feature.job.*` entry in configuration.
  *
  * @param enable Enable scheduled job to run.
  */
final case class JobConfig(enable: Boolean)
