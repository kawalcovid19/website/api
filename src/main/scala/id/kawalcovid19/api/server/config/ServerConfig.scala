package id.kawalcovid19.api.server.config

/** Web server configuration.
  *
  * Maps to `server.*` entry in configuration.
  *
  * @param port Web server port.
  */
final case class ServerConfig(port: Int)
