package id.kawalcovid19.api.server.config

/** Sentry logging configuration.
  *
  * Maps to `sentry.*` entry in configuration.
  *
  * See:
  *   - https://docs.sentry.io/clients/java/config/#setting-the-dsn
  *   - https://docs.sentry.io/clients/java/integrations/#logback
  *
  * @param dsn Sentry Data Source Name to send logs to.
  * @param token Bearer token for API calls authentication.
  * @param host Sentry API host.
  * @param organisation Application organisation slug entry in Sentry.
  */
final case class SentryConfig(dsn: Option[String], token: Option[String], host: String, organisation: String)
