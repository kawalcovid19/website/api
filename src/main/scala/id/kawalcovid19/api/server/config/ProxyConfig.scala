package id.kawalcovid19.api.server.config

import scala.concurrent.duration.FiniteDuration

/** Proxy routes configuration.
  *
  * Maps to `proxy.*` entry in configuration.
  *
  * @param routes Proxy routes (raw) value, if provided.
  * @param ttl Cache Time to Live, if provided.
  */
final case class ProxyConfig(routes: Option[String], ttl: Option[FiniteDuration]) {
  import ProxyConfig._

  val routesMap: Map[String, String] =
    routes
      .map { entries =>
        entries
          .split(SEPARATOR)
          .map(_.split(ASSIGNMENT).toSeq)
          .collect {
            case (key: String) +: (value: String) +: Nil => (key.trim, value.trim)
          }
          .toMap
      }
      .getOrElse(Map.empty[String, String])
}

object ProxyConfig {

  private[config] final val SEPARATOR  = '|'
  private[config] final val ASSIGNMENT = '='
}
