package id.kawalcovid19.api.server.config

/** Application feature flags.
  *
  * Maps to `feature.*` entry in configuration.
  *
  * @param job Scheduled Job feature configuration.
  */
final case class FeatureConfig(job: JobConfig)
