package id.kawalcovid19.api.server.config

import scala.io.Source

import pureconfig.error.ConfigReaderException

import id.kawalcovid19.api.server.helper.AutoCloseableSupport

/** Main application configuration.
  *
  * Maps to properties as defined in `application.conf`
  *
  * @param content Content service API configuration.
  * @param feature Feature flags configuration.
  * @param google Google Auth configuration.
  * @param mysql MySQL / MariaDB connection configuration.
  * @param proxy Proxy routes configuration.
  * @param runtimeEnv Runtime environment description, either dev or production.
  * @param sentry Sentry logging configuration.
  * @param server Server configuration.
  */
final case class AppConfig(
    content: ContentConfig,
    feature: FeatureConfig,
    google: GoogleConfig,
    mysql: MySqlConfig,
    proxy: ProxyConfig,
    runtimeEnv: String,
    sentry: SentryConfig,
    server: ServerConfig
) extends AutoCloseableSupport {
  import AppConfig._

  /** Application version, reads from bundled version file */
  lazy val version: String =
    using(Source.fromResource(VERSION_FILE))(_.mkString)
}

object AppConfig {

  private[config] final val VERSION_FILE = "VERSION.txt"

  /** Load application configuration.
    *
    * Uses `pureconfig`'s generic derivation, requires `pureconfig.generic.auto._` import in scope.
    *
    * @return Application Configuration.
    * @throws ConfigReaderException[AppConfig] if read failed.
    */
  def load: AppConfig = {
    import pureconfig._
    import pureconfig.generic.auto._

    loadConfigOrThrow[AppConfig]
  }
}
