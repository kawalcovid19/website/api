package id.kawalcovid19.api.server.config

/** MySQL or MariaDB connection configuration.
  *
  * Maps to `mysql.*` entry in configuration.
  *
  * @param url Database URL.
  * @param username Database Username.
  * @param password Database password.
  */
final case class MySqlConfig(url: String, username: String, password: String)
