package id.kawalcovid19.api.server.config

/** Content service API configuration.
  *
  * Maps to `content.*` entry in configuration.
  *
  * @param wpBaseUrl Base URL of the WordPress instance API.
  * @param wpEndpoint WordPress endpoints.
  * @param maxPostsFetch Maximum number of records to fetch.
  * @param defaultPostsResponse Default number of records to fetch.
  */
final case class ContentConfig(
    wpBaseUrl: String,
    wpEndpoint: WordPressEndpoint,
    maxPostsFetch: Int,
    defaultPostsResponse: Int
) {
  // default value from WordPress if this value is not specified will be only fetch 10 posts
  final private val postsParam = if (maxPostsFetch > 0) "?per_page=" + maxPostsFetch else ""

  final val postsUrl    = wpBaseUrl + wpEndpoint.posts + postsParam
  final val categoryUrl = wpBaseUrl + wpEndpoint.categories
  final val tagsUrl     = wpBaseUrl + wpEndpoint.tags
  final val usersUrl    = wpBaseUrl + wpEndpoint.users
}

/** Content service API configuration.
  *
  * Maps to `content.wp-endpoint.*` entry in configuration.
  *
  * @param posts endpoint for fetching posts.
  * @param categories endpoint for fetching categories.
  * @param tags endpoint for fetching tags.
  * @param users endpoint for fetching users.
  */
final case class WordPressEndpoint(posts: String, categories: String, tags: String, users: String)
