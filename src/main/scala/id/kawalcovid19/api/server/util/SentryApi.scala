package id.kawalcovid19.api.server.util

import scala.concurrent.{ExecutionContext, Future}

import akka.actor.ActorSystem
import sttp.client._
import sttp.client.akkahttp.AkkaHttpBackend
import sttp.client.circe._
import io.circe.generic.auto._
import sttp.model.StatusCode

import id.kawalcovid19.api.server.config.AppConfig

trait SentryApi {

  /** List all available deployments for a given version.
    *
    * @param organisation Organisation slug in Sentry.
    * @param version Current application version.
    * @param ec Execution Context for task scheduling.
    * @return
    */
  def listDeploys(organisation: String, version: String)(
      implicit ec: ExecutionContext
  ): Future[Either[Unit, Vector[SentryDeployment]]]

  /** Mark the given version as deployed.
    *
    * @param environment Current environment context.
    * @param organisation Organisation slug in Sentry.
    * @param version Current application version.
    * @param ec Execution Context for task scheduling.
    * @return Nothing if successful.
    */
  def createDeploy(environment: String, organisation: String, version: String)(
      implicit ec: ExecutionContext
  ): Future[Unit]
}

class SentryApiClient(token: String, host: String)(
    implicit system: ActorSystem
) extends SentryApi {

  private final val log = system.log

  private implicit val backend = AkkaHttpBackend.usingActorSystem(system)

  def listDeploys(organisation: String, version: String)(
      implicit ec: ExecutionContext
  ): Future[Either[Unit, Vector[SentryDeployment]]] =
    for {
      _ <- Future.unit
      uri = uri"${host}/api/0/organizations/${organisation}/releases/${version}/deploys/"
      req = sttp.client.basicRequest.auth
        .bearer(token)
        .response(asJson[Vector[SentryDeployment]])
        .get(uri)
      res <- req.send()
    } yield res match {
      case Response(Right(deploys), _, _, _, _) =>
        log.info("[listDeploys] Found existing deployment records for {}", version)
        Right(deploys)
      case Response(Left(_), StatusCode.NotFound, _, _, _) =>
        log.info("[listDeploys] No existing deployments for {}", version)
        Right(Vector.empty)
      case Response(Left(msg), code, _, _, _) =>
        log.error("[listDeploys] Unable to parse response (HTTP {}): {}", code.code, msg)
        Left(())
      case Response(_, _, _, _, _) =>
        log.error("[listDeploys] Unexpected error while listing existing deployments")
        Left(())
    }

  def createDeploy(environment: String, organisation: String, version: String)(
      implicit ec: ExecutionContext
  ): Future[Unit] =
    for {
      _ <- Future.successful(log.debug(s"[createDeploy] Creating deployment entry in Sentry for '${version}'"))
      payload = Map("environment" -> environment)
      uri     = uri"${host}/api/0/organizations/${organisation}/releases/${version}/deploys/"
      req     = sttp.client.basicRequest.auth.bearer(token).body(payload).post(uri)
      res <- req.send()
    } yield res match {
      case Response(Right(_), _, _, _, _) =>
        log.info("[createDeploy] Deployment successfully marked")
      case Response(Left(msg), _, _, _, _) =>
        log.error("[createDeploy] Unable to parse response: {}", msg)
      case Response(_, _, _, _, _) =>
        log.error("[createDeploy] Unexpected error while marking deployment")
    }
}

object SentryApiClient {

  def apply(config: AppConfig)(implicit system: ActorSystem): Option[SentryApiClient] = {
    config.sentry.token
      .map { bearerToken =>
        new SentryApiClient(bearerToken, config.sentry.host)
      }
  }
}
