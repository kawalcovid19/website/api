package id.kawalcovid19.api.server.util

final case class SentryDeployment(
    id: String,
    name: Option[String],
    url: Option[String],
    environment: String,
    dateStarted: Option[String],
    dateFinished: Option[String]
)
