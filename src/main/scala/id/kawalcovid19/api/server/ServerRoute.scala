package id.kawalcovid19.api.server

import akka.http.scaladsl.coding.{Deflate, Gzip, NoCoding}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import com.github.swagger.akka.SwaggerHttpService

import id.kawalcovid19.api.caseHistory.CaseHistoryRoute
import id.kawalcovid19.api.content.ContentRoute
import id.kawalcovid19.api.proxy.ProxyRoute
import id.kawalcovid19.api.server.error.ErrorHandlerRoute
import id.kawalcovid19.api.server.health.HealthCheckRoute

class ServerRoute(
    errorHandler: ErrorHandlerRoute.type,
    healthCheck: HealthCheckRoute,
    patientInfoSummary: CaseHistoryRoute,
    contentRoute: ContentRoute,
    proxy: ProxyRoute,
    swagger: SwaggerHttpService
) {

  final val route: Route =
    encodeResponseWith(Gzip, Deflate, NoCoding) {
      handleExceptions(errorHandler.route) {
        pathPrefix("api") {
          patientInfoSummary.route ~
            contentRoute.route
        } ~
          proxy.route ~
          healthCheck.route ~
          swagger.routes
      }
    }
}
