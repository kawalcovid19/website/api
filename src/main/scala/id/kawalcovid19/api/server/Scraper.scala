package id.kawalcovid19.api.server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import id.nolimit.sqlLayer.connections.ConnectionPool
import id.kawalcovid19.api.caseHistory.models.{CaseSummary, Patient, SheetRange, SpreadSheet}
import id.kawalcovid19.api.caseHistory.pipelines.{Runner, SuperHttpPoolQueue}
import id.kawalcovid19.api.server.auth.{GoogleApiKey, GoogleServiceAccount}
import id.kawalcovid19.api.server.config.AppConfig
import id.kawalcovid19.api.server.helper.GracefulExitSupport

object Scraper extends GracefulExitSupport {

  def main(args: Array[String]): Unit = {
    implicit val system       = ActorSystem("kawalcovid19-scraper")
    implicit val materializer = ActorMaterializer()
    implicit val ec           = system.dispatcher
    implicit val pool         = ConnectionPool.create("main", "mysql")

    val config               = AppConfig.load
    val googleApiKey         = GoogleApiKey(config.google.apiKey)
    val googleServiceAccount = GoogleServiceAccount.fromConfig(config.google)
    val poolQueue            = new SuperHttpPoolQueue(10)

    val patientSpreadSheet =
      SpreadSheet("1ma1T9hWbec1pXlwZ89WakRk-OfVUQZsOCFl4FwZxzVw", SheetRange("Daftar Kasus", "A1", "U200"), 1)

    val summarySpreadSheet =
      SpreadSheet("1McDKkM-BO8Evbhr7iAjraCoCT5V44d-8oHUZpDpUuh0", SheetRange("Sheet1", "A1", "E100"), 0)

    val patientIngest = Runner
      .runIngestionPipeline[Patient](
        poolQueue,
        patientSpreadSheet,
        apiKey = googleApiKey,
        serviceAccountOpt = googleServiceAccount
      )

    val summaryIngest = Runner
      .runIngestionPipeline[CaseSummary](
        poolQueue,
        summarySpreadSheet,
        Some(s"""
           |confirmed = VALUES(confirmed),
           |recovered = VALUES(recovered),
           |deceased = VALUES(deceased),
           |created_at = VALUES(created_at)
           |""".stripMargin),
        googleApiKey,
        googleServiceAccount
      )

    (for {
      _ <- patientIngest
      _ <- summaryIngest
    } yield ()).gracefulExit()
  }
}
