package id.kawalcovid19.api.server.error

import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport

object ErrorHandlerRoute extends FailFastCirceSupport {
  import io.circe._
  import io.circe.generic.semiauto._

  final val route =
    ExceptionHandler {
      case other =>
        complete(ErrorResponse(other.getMessage))
    }

  private final case class ErrorResponse(reason: String)
  private final object ErrorResponse {
    implicit val encoder: Encoder[ErrorResponse] = deriveEncoder[ErrorResponse]
  }
}
