package id.kawalcovid19.api.server

import scala.concurrent.{ExecutionContext, Future}
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import id.nolimit.sqlLayer.connections.ConnectionPool
import io.sentry.Sentry
import id.kawalcovid19.api.caseHistory.pipelines.{ShowSummary, SuperHttpPoolQueue}
import id.kawalcovid19.api.caseHistory.{CaseHistoryJob, CaseHistoryRoute}
import id.kawalcovid19.api.content.{ContentRoute, ContentServiceImpl, HttpRequestClientImpl}
import id.kawalcovid19.api.proxy.{ProxyRoute, ProxyServiceSttpImplementation}
import id.kawalcovid19.api.server.config.AppConfig
import id.kawalcovid19.api.server.error.ErrorHandlerRoute
import id.kawalcovid19.api.server.health.HealthCheckRoute
import id.kawalcovid19.api.server.openapi.OpenApiDocumentationRoute
import id.kawalcovid19.api.server.util.SentryApiClient

object Server {

  /** Production environment flag */
  private final val PRODUCTION_ENV = "production"

  def main(args: Array[String]): Unit = {
    implicit val system       = ActorSystem("kawalcovid19")
    implicit val materializer = ActorMaterializer()
    implicit val ec           = system.dispatcher
    implicit val pool         = ConnectionPool.create("main", "mysql")

    val config = AppConfig.load

    val proxyService = new ProxyServiceSttpImplementation(config.proxy.routesMap, config.proxy.ttl)

    val httpClient     = new HttpRequestClientImpl
    val contentService = new ContentServiceImpl(httpClient, config.content)

    val httpRoute = new ServerRoute(
      ErrorHandlerRoute,
      new HealthCheckRoute,
      new CaseHistoryRoute(ShowSummary),
      new ContentRoute(contentService),
      new ProxyRoute(proxyService),
      new OpenApiDocumentationRoute(config.version)
    )

    // Initialise Scheduled Job if feature flag is enabled
    if (config.feature.job.enable) {
      system.log.info("[main] Scheduled Job is enabled, configuring Quartz scheduler")
      val poolQueue      = new SuperHttpPoolQueue(20)
      val caseHistoryJob = system.actorOf(CaseHistoryJob.props(poolQueue, config.google, pool))
      val scheduler      = QuartzSchedulerExtension(system)

      scheduler.schedule("Every10Minutes", caseHistoryJob, CaseHistoryJob.StartIngest)
    }

    for {
      _ <- sentryInit(config)
      _ <- sentryMarkDeploy(config, force = false)
      _ <- startServer(httpRoute.route, config.server.port)
    } yield ()
  }

  /** Initialise Sentry if `SENTRY_DSN` environment variable is provided.
    *
    * @param config Application config.
    * @param system Actor System.
    * @return Nothing if successful.
    */
  private[this] def sentryInit(config: AppConfig)(implicit system: ActorSystem): Future[Unit] =
    config.sentry.dsn.fold(Future.unit) { dsn =>
      system.log.info("[sentryInit] Sentry DSN is provided, initialising Sentry Client API")

      val dsnWithOpts = s"${dsn}?release=${config.version}&environment=${config.runtimeEnv}"

      Sentry.init(dsnWithOpts)

      Future.unit
    }

  /** Mark production deployment in Sentry (i.e. tag version as released).
    *
    * @param config Application config.
    * @param force True to force deployment marking.
    * @param system Actor system.
    * @param ec Execution Context for task scheduling.
    * @return Nothing if successful.
    */
  private[this] def sentryMarkDeploy(
      config: AppConfig,
      force: Boolean
  )(implicit system: ActorSystem, ec: ExecutionContext): Future[Unit] = {
    val isProduction = config.runtimeEnv.equalsIgnoreCase(PRODUCTION_ENV)
    val clientOpt    = SentryApiClient(config)
    val organisation = config.sentry.organisation
    val version      = config.version

    (isProduction, clientOpt) match {
      case (true, Some(client)) =>
        for {
          _ <- Future.successful(
            system.log.info("[sentryMarkDeploy] Production environment flag is set, marking Sentry deployment")
          )
          dsE <- client.listDeploys(organisation, version)
          shouldMark = dsE.fold(_ => false, ds => ds.isEmpty)
          res <- {
            if (force || shouldMark) client.createDeploy(PRODUCTION_ENV, organisation, version)
            else Future.unit
          }
        } yield res
      case (true, _) =>
        system.log.warning(
          "[sentryMarkDeploy] Production environment flag is set, but Sentry API client configuration is incomplete"
        )
        Future.unit
      case (_, _) =>
        Future.unit
    }
  }

  /** Start akka-http server.
    *
    * @param route Server routes.
    * @param port Server port.
    * @param system Actor System.
    * @param materializer Actor System materializer.
    * @return akka-http server binding if successful.
    */
  private[this] def startServer(
      route: Route,
      port: Int
  )(implicit system: ActorSystem, materializer: ActorMaterializer): Future[ServerBinding] = {
    system.log.info("[main] Starting server at port {}", port)

    Http().bindAndHandle(route, "0.0.0.0", port)
  }
}
