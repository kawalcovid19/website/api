package id.kawalcovid19.api.server.health

import javax.ws.rs.{GET, Path}

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse

class HealthCheckRoute extends FailFastCirceSupport {
  import io.circe._
  import io.circe.generic.semiauto._

  final val route: Route = health

  @Path("health")
  @GET
  @Operation(
    summary = "Health Check endpoint",
    description = "Health Check endpoint to enable periodic testing for a service's instance availability.",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Service status response",
        content = Array(
          new Content(
            mediaType = "application/json",
            schema = new Schema(implementation = classOf[HealthCheckResponse])
          )
        )
      )
    )
  )
  def health: Route =
    pathPrefix("health") {
      get {
        complete(HealthCheckResponse())
      }
    }

  private final case class HealthCheckResponse(status: String = "UP")
  private final object HealthCheckResponse {
    implicit val encoder: Encoder[HealthCheckResponse] = deriveEncoder[HealthCheckResponse]
  }
}
