package id.kawalcovid19.api.server.tools.support

import java.nio.file.{Path, Paths}

import org.rogach.scallop.{DefaultConverters, ValueConverter}

trait ScallopConfCustomConverter extends DefaultConverters {

  implicit val pathConverter: ValueConverter[Path] = stringConverter.map(Paths.get(_))
}
