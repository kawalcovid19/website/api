package id.kawalcovid19.api.server

import id.kawalcovid19.api.server.helper.GracefulExitSupport
import id.kawalcovid19.api.server.tools.support.ScallopConfCustomConverter

package object tools extends GracefulExitSupport with ScallopConfCustomConverter
