package id.kawalcovid19.api.server.helper

import java.io.Closeable

trait AutoCloseableSupport {

  /** Convenience syntax for working with closeable resource.
    *
    * @param res Closeable resource.
    * @param fn Function that will operate using the given resource.
    * @tparam T Resource type parameter.
    * @tparam U Result type parameter.
    * @return Resource result.
    */
  protected def using[T <: Closeable, U](res: T)(fn: T => U): U =
    try fn(res)
    finally res.close()
}
