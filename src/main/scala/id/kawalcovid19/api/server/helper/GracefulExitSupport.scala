package id.kawalcovid19.api.server.helper

import scala.concurrent.{ExecutionContext, Future}

import akka.actor.ActorSystem

trait GracefulExitSupport {

  implicit class GracefulExitOnFutureCompleteExtension[T](future: Future[T])(implicit system: ActorSystem) {

    /** Perform graceful exit / shutdown and handle the result, for both success and error.
      *
      * @param onSuccess Function handler for successful termination, invoked prior to shutdown.
      * @param onError Function handler for erroneous termination, invoked prior to shutdwn.
      */
    def gracefulExit(onSuccess: T => Unit)(onError: Throwable => Unit): Unit = {
      implicit val ec: ExecutionContext = system.dispatcher

      future
        .map { item =>
          onSuccess(item)
        }
        .recover {
          case e: Throwable => onError(e)
        }
        .flatMap(_ => system.terminate())
        .onComplete(_ => sys.exit())
    }

    /** Perform graceful exit / shutdown.
      *
      * Log the result, for either success or error.
      */
    def gracefulExit(): Unit = {
      gracefulExit(_ => system.log.info("Operation is done!")) { err =>
        system.log.error(err, "Error on running operation!")
      }
    }
  }
}
