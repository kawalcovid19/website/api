package id.kawalcovid19.api.server.openapi

import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.{Info, License}

import id.kawalcovid19.api.caseHistory.CaseHistoryRoute
import id.kawalcovid19.api.content.ContentRoute
import id.kawalcovid19.api.server.health.HealthCheckRoute

class OpenApiDocumentationRoute(version: String) extends SwaggerHttpService {

  val apiClasses: Set[Class[_]] = Set(
    classOf[HealthCheckRoute],
    classOf[CaseHistoryRoute],
    classOf[ContentRoute]
  )

  override val info = Info(
    title = "Kawal COVID-19 API Service",
    description = "API service for Kawal COVID-19 website.",
    version = version,
    license = Some(License(name = "MIT", url = "https://opensource.org/licenses/MIT")),
    termsOfService = "https://kawalcovid19.id/kebijakan-privasi"
  )

  override val unwantedDefinitions = Seq(
    "Function1",
    "Function1RequestContextFutureRouteResult"
  )
}
