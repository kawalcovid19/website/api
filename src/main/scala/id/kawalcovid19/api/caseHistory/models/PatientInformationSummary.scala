package id.kawalcovid19.api.caseHistory.models

import io.circe._
import io.circe.generic.semiauto._
import org.joda.time.DateTime

import id.kawalcovid19.api.caseHistory.commons._

final case class PatientInformationSummary(
    confirmed: Int,
    recovered: Int,
    deceased: Int,
    lastUpdated: Option[DateTime],
    nationalityMap: Map[String, Long],
    provinceMap: Map[String, Long],
    clusterMap: Map[String, Long],
    genderMap: Map[String, Long]
) {
  import PatientInformationSummary._

  val activeCare: Int = confirmed - recovered - deceased

  /** Append Patient data into Patient Information Summary aggregate.
    *
    * @param patient Patient data.
    * @return Patient Information Summary aggregate totals.
    */
  def append(patient: Patient): PatientInformationSummary = {
    val newNationalityMap = appendOptionToMap(patient.nationality, nationalityMap)
    val newProvinceMap    = appendOptionToMap(patient.province, provinceMap)
    val newClusterMap     = appendOptionToMap(patient.cluster, clusterMap)
    val newGenderMap      = appendOptionToMap(patient.gender, genderMap)

    copy(
      confirmed = confirmed + patient.declaredPositiveAt.fold(0)(_ => 1),
      recovered = recovered + patient.recoveredAt.fold(0)(_ => 1),
      deceased = deceased + patient.deceasedAt.fold(0)(_ => 1),
      lastUpdated = updateTimestamp(lastUpdated, patient.createdAt),
      nationalityMap = newNationalityMap,
      provinceMap = newProvinceMap,
      clusterMap = newClusterMap,
      genderMap = newGenderMap
    )
  }
}

object PatientInformationSummary {

  def apply(summary: CaseSummary): PatientInformationSummary = {
    apply(
      summary.confirmed,
      summary.recovered,
      summary.deceased,
      Some(summary.createdAt),
      nationalityMap = Map.empty,
      provinceMap = Map.empty,
      clusterMap = Map.empty,
      genderMap = Map.empty
    )
  }

  private[models] final val EMPTY =
    PatientInformationSummary(
      confirmed = 0,
      recovered = 0,
      deceased = 0,
      lastUpdated = None,
      nationalityMap = Map.empty,
      provinceMap = Map.empty,
      clusterMap = Map.empty,
      genderMap = Map.empty
    )

  /** Increment data point count, if data point exists, to an existing map.
    *
    * @param opt Data point (if exists).
    * @param map Map to increment data point count to.
    * @return Map with data point count incremented.
    */
  private[models] def appendOptionToMap(opt: Option[String], map: Map[String, Long]): Map[String, Long] =
    opt.fold(map) { entry =>
      val normalisedKey = entry.toLowerCase
      val value         = map.get(normalisedKey).map(_ + 1L).getOrElse(1L)
      map.updated(normalisedKey, value)
    }

  /** Returns the earliest timestamp, comparing the current one (if exists) with the provided one.
    *
    * @param lastUpdated Current timestamp.
    * @param update Provided update timestamp.
    * @return Earliest timestamp between current and provided.
    */
  private[models] def updateTimestamp(lastUpdated: Option[DateTime], update: DateTime): Option[DateTime] =
    lastUpdated match {
      case lu @ Some(ts) if ts.isBefore(update) => lu
      case Some(_)                              => Some(update)
      case None                                 => Some(update)
    }

  implicit val emptyProvider: EmptyProvider[PatientInformationSummary] =
    new EmptyProvider[PatientInformationSummary] {
      override def empty: PatientInformationSummary = EMPTY
    }
}

final case class PatientSummaryMetadata(lastUpdatedAt: DateTime)
object PatientSummaryMetadata extends DateTimeSupport {
  implicit val encoder: Encoder[PatientSummaryMetadata] = deriveEncoder[PatientSummaryMetadata]
}
final case class PatientSummaryResponse(
    confirmed: Int,
    recovered: Int,
    deceased: Int,
    activeCare: Int,
    metadata: PatientSummaryMetadata
)
object PatientSummaryResponse {

  def apply(confirmed: Int, recovered: Int, deceased: Int, lastUpdatedAt: DateTime): PatientSummaryResponse = {
    PatientSummaryResponse(
      confirmed,
      recovered,
      deceased,
      confirmed - recovered - deceased,
      PatientSummaryMetadata(lastUpdatedAt)
    )
  }

  implicit val encoder: Encoder[PatientSummaryResponse] = deriveEncoder[PatientSummaryResponse]
}
