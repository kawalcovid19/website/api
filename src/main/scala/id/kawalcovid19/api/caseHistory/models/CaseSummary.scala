package id.kawalcovid19.api.caseHistory.models

import anorm.JodaParameterMetaData._
import anorm.RowParser
import id.nolimit.sqlLayer.Sql.ParameterMap
import id.nolimit.sqlLayer.primaryKey.IdName
import id.nolimit.sqlLayer.{TableName, _}
import org.joda.time.DateTime
import play.api.libs.json.{Json, Writes}

import id.kawalcovid19.api.caseHistory.commons._
import id.kawalcovid19.api.caseHistory.parsers.{Parser, _}

case class CaseSummary(
    confirmed: Int,
    recovered: Int,
    deceased: Int,
    timestamp: DateTime,
    createdAt: DateTime
) {
  val activeCare = confirmed - recovered - deceased
}

object CaseSummary extends DateTimeSupport {
  implicit val emptyProvider: EmptyProvider[CaseSummary] = new EmptyProvider[CaseSummary] {
    override def empty = CaseSummary(0, 0, 0, DateTime.now(), DateTime.now())
  }

  implicit val tableName: TableName[CaseSummary] = new TableName[CaseSummary] {
    override val name: String = "patient_summaries"
  }

  implicit val idName: IdName[CaseSummary] = new IdName[CaseSummary] {
    override val columnName: String = "patient_summaries.id"
  }

  implicit val parser: Parser[CaseSummary] = new Parser[CaseSummary] {
    override def read(map: Map[String, String]) = {
      for {
        confirmed <- (map \ "confirmed").asEither[Int]
        recovered <- (map \ "recovered").asEither[Int]
        deceased  <- (map \ "death").asEither[Int]
        timestamp <- (map \ "date").asEither[DateTime]
      } yield {
        CaseSummary(
          confirmed,
          recovered,
          deceased,
          timestamp.withYear(2020),
          DateTime.now()
        )
      }
    }
  }

  implicit val rowParser: RowParser[CaseSummary] = {
    import anorm.SqlParser._
    import anorm._
    int("confirmed") ~
      int("recovered") ~
      int("deceased") ~
      date("timestamp") ~
      date("created_at") map {
      case confirmed ~ recovered ~ deceased ~ timestamp ~ createdAt =>
        CaseSummary(confirmed, recovered, deceased, new DateTime(timestamp), new DateTime(createdAt))
    }
  }

  implicit val toInsertMap: CaseSummary => ParameterMap = { summary =>
    {
      ParameterMap(
        "confirmed" ~> summary.confirmed,
        "recovered" ~> summary.recovered,
        "deceased" ~> summary.deceased,
        "timestamp" ~> summary.timestamp,
        "created_at" ~> DateTime.now()
      )
    }
  }

  implicit val jsonWrites: Writes[CaseSummary] = new Writes[CaseSummary] {
    override def writes(o: CaseSummary) = Json.obj(
      "confirmed"  -> Json.obj("value" -> o.confirmed),
      "recovered"  -> Json.obj("value" -> o.recovered),
      "deaths"     -> Json.obj("value" -> o.deceased),
      "activeCare" -> Json.obj("value" -> o.activeCare),
      "metadata" -> Json.obj(
        "lastUpdatedAt" -> o.createdAt
      )
    )
  }
}
