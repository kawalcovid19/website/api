package id.kawalcovid19.api.caseHistory.models

import anorm.JodaParameterMetaData._
import anorm.ParameterValue._
import anorm.ToStatement._
import anorm._
import id.nolimit.sqlLayer.Sql.ParameterMap
import id.nolimit.sqlLayer.primaryKey.IdName
import id.nolimit.sqlLayer.{TableName, _}
import org.joda.time.DateTime
import play.api.libs.json.{Json, Writes}

import id.kawalcovid19.api.caseHistory.commons._
import id.kawalcovid19.api.caseHistory.parsers._

/**
  * "Usia",
  *"JK",
  *"WN",
  *"Provinsi",
  *"Kota",
  *"Kontak dgn pasien COVID-19",
  *"Mulai Gejala",
  *"Mulai Diisolasi",
  *"Positif",
  *"Sembuh",
  *"Meninggal",
  *"Dirawat di",
  *"Jenis kasus",
  *"Kontak ref",
  *"Keterangan",
  *"Referensi",
  *"Lama inkubasi",
  *"Status",
  */
case class Patient(
    caseId: String,
    age: Option[Int],
    gender: Option[String],
    nationality: Option[String],
    province: Option[String],
    city: Option[String],
    firstContactAt: Option[DateTime],
    firstObservedSymptomAt: Option[DateTime],
    firstIsolationAt: Option[DateTime],
    declaredPositiveAt: Option[DateTime],
    recoveredAt: Option[DateTime],
    deceasedAt: Option[DateTime],
    hospitalizedIn: Option[String],
    locationType: Option[String],
    contactReference: Option[String],
    notes: Option[String],
    incubationPeriod: Option[Int],
    finalStatus: Option[String],
    createdAt: DateTime,
    cluster: Option[String]
) {
  override def toString: String =
    s"Patient(id = ${caseId}, age = ${age}, gender = ${gender}, nationality = ${nationality}, province = ${province}, city = ${city}, fca = ${firstContactAt}, fosa = ${firstObservedSymptomAt}, fia = ${firstIsolationAt}, dpa = ${declaredPositiveAt}, recovered = ${recoveredAt}, deceased = ${deceasedAt}, hospital = ${hospitalizedIn}, location = ${locationType}, contactRef = ${contactReference}, notes = ${notes}, incubationPeriod = ${incubationPeriod}, status = ${finalStatus}), cluster = ${cluster}"
}

object Patient {

  implicit class OptStringCleaner(opt: Option[String]) {
    def sanitize =
      opt
        .map { str =>
          str.replaceAll("^\\s*-\\s*$", "").trim
        }
        .filter(_.nonEmpty)
  }

  implicit val tableName: TableName[Patient] = new TableName[Patient] {
    override val name: String = "patients"
  }

  implicit val idName: IdName[Patient] = new IdName[Patient] {
    override val columnName: String = "patients.id"
  }

  implicit val rowParser: RowParser[Patient] = {
    import anorm.SqlParser._
    import anorm._
    str("case_id") ~
      int("age").? ~
      str("nationality").? ~
      str("gender").? ~
      str("province").? ~
      str("city").? ~
      date("first_contact_at").? ~
      date("first_symptoms_at").? ~
      date("first_isolation_at").? ~
      date("declared_positive_at").? ~
      date("recovered_at").? ~
      date("deceased_at").? ~
      str("hospitalized_in").? ~
      str("location_type").? ~
      str("contact_reference").? ~
      str("notes").? ~
      int("incubation_period").? ~
      str("final_status").? ~
      date("created_at") ~
      str("cluster").? map {
      case caseId ~
            ageOpt ~
            nationalityOpt ~
            genderOpt ~
            provinceOpt ~
            cityOpt ~
            fcaOpt ~
            fosaOpt ~
            fiaOpt ~
            dpaOpt ~
            recoveredAt ~
            deceasedAt ~
            hospitalizedIn ~
            locationType ~
            contactRef ~
            notes ~
            incubationPeriodOpt ~
            finalStatus ~
            createdAt ~
            cluster =>
        Patient(
          caseId,
          ageOpt,
          genderOpt,
          nationalityOpt,
          provinceOpt,
          cityOpt,
          fcaOpt.map(new DateTime(_)),
          fosaOpt.map(new DateTime(_)),
          fiaOpt.map(new DateTime(_)),
          dpaOpt.map(new DateTime(_)),
          recoveredAt.map(new DateTime(_)),
          deceasedAt.map(new DateTime(_)),
          hospitalizedIn,
          locationType,
          contactRef,
          notes,
          incubationPeriodOpt,
          finalStatus,
          new DateTime(createdAt),
          cluster
        )
    }
  }

  implicit val writes: Writes[Patient] = Json.writes[Patient]

  implicit val mapParser: Parser[Patient] = new Parser[Patient] {
    override def read(map: Map[String, String]) =
      for {
        caseId              <- (map \ "no").asEither[String]
        ageOpt              <- (map \ "usia").asEitherOpt[Int]
        genderOpt           <- (map \ "jk").asEitherOpt[String].map(_.filter(_.nonEmpty))
        nationalityOpt      <- (map \ "wn").asEitherOpt[String].map(_.filter(_.nonEmpty))
        provinceOpt         <- (map \ "provinsi").asEitherOpt[String].map(_.filter(_.nonEmpty))
        cityOpt             <- (map \ "kota").asEitherOpt[String].map(_.filter(_.nonEmpty))
        fcaOpt              <- (map \ "kontak dgn pasien covid-19").asEitherOpt[DateTime]
        fosaOpt             <- (map \ "mulai gejala").asEitherOpt[DateTime]
        fiaOpt              <- (map \ "mulai diisolasi").asEitherOpt[DateTime]
        dpaOpt              <- (map \ "positif").asEitherOpt[DateTime]
        recoveredAt         <- (map \ "sembuh").asEitherOpt[DateTime]
        deceasedAt          <- (map \ "meninggal").asEitherOpt[DateTime]
        hospitalizedIn      <- (map \ "dirawat di").asEitherOpt[String].map(_.filter(_.nonEmpty))
        locationType        <- (map \ "jenis kasus").asEitherOpt[String].map(_.filter(_.nonEmpty))
        contactRef          <- (map \ "sumber kontak").asEitherOpt[String]
        notes               <- (map \ "keterangan").asEitherOpt[String].map(_.filter(_.nonEmpty))
        incubationPeriodOpt <- (map \ "lama inkubasi").asEitherOpt[Int]
        finalStatus         <- (map \ "status").asEitherOpt[String].map(_.sanitize)
        cluster             <- (map \ "kluster").asEitherOpt[String].map(_.sanitize)

      } yield {
        Patient(
          caseId,
          ageOpt,
          genderOpt,
          nationalityOpt,
          provinceOpt,
          cityOpt,
          fcaOpt.map(_.withYear(2020)),
          fosaOpt.map(_.withYear(2020)),
          fiaOpt.map(_.withYear(2020)),
          dpaOpt.map(_.withYear(2020)),
          recoveredAt.map(_.withYear(2020)),
          deceasedAt.map(_.withYear(2020)),
          hospitalizedIn,
          locationType,
          contactRef,
          notes,
          incubationPeriodOpt,
          finalStatus,
          DateTime.now(),
          cluster
        )
      }
  }

  implicit val toInsertMap: Patient => ParameterMap = { patient =>
    {
      ParameterMap(
        "case_id" ~> patient.caseId,
        "age" ~> patient.age,
        "nationality" ~> patient.nationality,
        "gender" ~> patient.gender,
        "province" ~> patient.province,
        "city" ~> patient.city,
        "first_contact_at" ~> patient.firstContactAt,
        "first_symptoms_at" ~> patient.firstObservedSymptomAt,
        "first_isolation_at" ~> patient.firstIsolationAt,
        "declared_positive_at" ~> patient.declaredPositiveAt,
        "recovered_at" ~> patient.recoveredAt,
        "deceased_at" ~> patient.deceasedAt,
        "hospitalized_in" ~> patient.hospitalizedIn,
        "location_type" ~> patient.locationType,
        "contact_reference" ~> patient.contactReference,
        "incubation_period" ~> patient.incubationPeriod,
        "notes" ~> patient.notes,
        "final_status" ~> patient.finalStatus,
        "created_at" ~> DateTime.now(),
        "cluster" ~> patient.cluster
      )
    }
  }
}
