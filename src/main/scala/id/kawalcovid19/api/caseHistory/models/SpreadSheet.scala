package id.kawalcovid19.api.caseHistory.models

case class SpreadSheet(sheetId: String, range: SheetRange, headerPosition: Int)
