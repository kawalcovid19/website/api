package id.kawalcovid19.api.caseHistory.models

case class SheetRange(sheetName: String, fromCell: String, toCell: String) {
  def formatted = s"${sheetName}!${fromCell}:${toCell}"
}
