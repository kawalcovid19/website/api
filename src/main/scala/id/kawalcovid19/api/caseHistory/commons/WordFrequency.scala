package id.kawalcovid19.api.caseHistory.commons

import scala.util.Try

import play.api.libs.json._

case class WordFrequency(
    size: Long,
    word: String
) {
  def +(other: WordFrequency): WordFrequency = {
    copy(size = size + other.size)
  }
}

object WordFrequency {
  implicit val ordering: Ordering[WordFrequency] = new Ordering[WordFrequency] {
    override def compare(x: WordFrequency, y: WordFrequency) = y.size compareTo x.size
  }

  implicit val jsonParser: Format[WordFrequency] = new Format[WordFrequency] {
    override def writes(o: WordFrequency): JsValue = {
      Json.obj("name" -> o.word, "value" -> o.size)
    }

    override def reads(json: JsValue): JsResult[WordFrequency] = {
      Try {
        val name  = (json \ "name").as[String]
        val value = (json \ "value").as[Long]
        WordFrequency(value, name)
      }.fold(e => JsError(e.getMessage), r => JsSuccess(r))
    }
  }
}
