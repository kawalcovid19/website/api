package id.kawalcovid19.api.caseHistory.commons

import org.joda.time.DateTime
import play.api.libs.json.{JsValue, Json, Writes}

import id.kawalcovid19.api.caseHistory.commons.StopWatch.ElapsedTime

case class StopWatch() {
  val start = DateTime.now()
  def stop = {
    val end = DateTime.now()
    ElapsedTime(end.getMillis - start.getMillis, start, end)
  }
}

object StopWatch {
  case class ElapsedTime(duration: Long, start: DateTime, end: DateTime) {
    override def toString: String = s"ElapsedTime(${duration / 1000} seconds)"
  }
  object ElapsedTime extends DateTimeSupport {
    def neverStarted = ElapsedTime(0L, DateTime.now(), DateTime.now())
    implicit val jsonWrites: Writes[ElapsedTime] = new Writes[ElapsedTime] {
      override def writes(o: ElapsedTime): JsValue = {
        Json.obj(
          "timeElapsedInMs" -> o.duration,
          "start"           -> o.start,
          "end"             -> o.end
        )
      }
    }
  }
  def start = StopWatch()
}
