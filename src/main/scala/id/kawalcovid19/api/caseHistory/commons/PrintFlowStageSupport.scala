package id.kawalcovid19.api.caseHistory.commons

import scala.concurrent.duration._

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}

trait PrintFlowStageSupport {

  def flowRate[T](metric: T => Int = (_: T) => 1, outputDelay: FiniteDuration = 1 second) =
    Flow[T]
      .conflateWithSeed(metric(_)) { case (acc, x) ⇒ acc + metric(x) }
      .zip(Source.tick(outputDelay, outputDelay, NotUsed))
      .map(_._1.toDouble / outputDelay.toUnit(SECONDS))

  def printFlowRate[T](name: String, metric: T => Int = (_: T) => 1, outputDelay: FiniteDuration = 1 second)(
      implicit system: ActorSystem
  ): Flow[T, T, NotUsed] = {
    val log = system.log
    Flow[T]
      .alsoTo(
        flowRate[T](metric, outputDelay)
          .to(Sink.foreach(r => log.info(s"Rate($name): $r per $outputDelay ")))
      )
  }

  implicit class FlowPrinter[T, Mat](flow: Flow[T, T, Mat]) {
    def showFlowRate(name: String)(implicit system: ActorSystem): Flow[T, T, Mat] = {
      flow.via(printFlowRate[T](name))
    }
  }

  implicit class SourcePrinter[T, Mat](source: Source[T, Mat]) {
    def showFlowRate(name: String)(implicit system: ActorSystem): Source[T, Mat] = {
      source.via(printFlowRate[T](name))
    }
  }
}
