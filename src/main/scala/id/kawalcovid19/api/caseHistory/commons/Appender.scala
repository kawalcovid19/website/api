package id.kawalcovid19.api.caseHistory.commons

trait Appender[T] {
  def append(f1: T, f2: T): T
}

trait AppenderSupport {
  implicit class AppenderExtensionSupport[T](f1: T)(implicit appender: Appender[T]) {
    def append(f2: T) = {
      appender.append(f1, f2)
    }
  }
}

object Appender {
  def useLeft[T]: Appender[T] = new Appender[T] {
    override def append(f1: T, f2: T) = f1
  }
}
