package id.kawalcovid19.api.caseHistory.commons

trait EitherToSupport {

  implicit class SomethingToEither[A](f: => A) {
    def asLeft[R]: Either[A, R]  = Left(f)
    def asRight[L]: Either[L, A] = Right(f)
  }

  implicit class EitherLeftMap[L, R](either: Either[L, R]) {
    def leftMap[L2](f: L => L2) = {
      either match {
        case Right(value) => Right(value)
        case Left(value)  => Left(f(value))
      }
    }
  }

}
