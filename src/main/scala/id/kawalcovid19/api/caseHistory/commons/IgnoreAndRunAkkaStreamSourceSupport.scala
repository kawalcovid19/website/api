package id.kawalcovid19.api.caseHistory.commons

import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink, Source}

trait IgnoreAndRunAkkaStreamSourceSupport {

  implicit class IgnoreAndRunExtensionSupport[T](source: Source[T, Any])(implicit materializer: ActorMaterializer) {
    def ignoreAndRun =
      source
        .toMat(Sink.ignore)(Keep.right)
        .run()
  }

}
