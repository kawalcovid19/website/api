package id.kawalcovid19.api.caseHistory.commons

import scala.annotation.tailrec
import scala.reflect.ClassTag

import play.api.libs.json._

trait PlayJsonMultiPathSelectorSupport {

  case class MultiJsLookup(json: JsValue, head: JsLookupResult, other: Seq[JsLookupResult]) {

    def or(path: String): MultiJsLookup = {
      MultiJsLookup(json, head, other = other :+ (json \ path))
    }

    @tailrec
    final def as[T: Reads]: T = {
      head.validate[T] match {
        case JsSuccess(value, _) => value
        case err @ JsError(seq) =>
          if (other.isEmpty) throw JsResult.Exception(err)
          else MultiJsLookup(json, other.head, other.tail).as[T]
      }
    }

    @tailrec
    final def asOpt[T: Reads]: Option[T] = {
      head.validate[T] match {
        case JsSuccess(value, _) => Some(value)
        case err @ JsError(_) =>
          if (other.isEmpty) None
          else MultiJsLookup(json, other.head, other.tail).asOpt[T]
      }
    }

    @tailrec
    final def validate[T: Reads: ClassTag]: JsResult[T] = {
      head.validate[T] match {
        case success: JsSuccess[T] => success
        case err @ JsError(_) =>
          if (other.isEmpty) err
          else MultiJsLookup(json, other.head, other.tail).validate[T]
      }
    }

    @tailrec
    final def validateOpt[T: Reads: ClassTag]: JsResult[Option[T]] = {
      head.validateOpt[T] match {
        case success: JsSuccess[Option[T]] => success
        case err @ JsError(_) =>
          if (other.isEmpty) err
          else MultiJsLookup(json, other.head, other.tail).validateOpt[T]
      }
    }
  }

  implicit class PlayJsonMultiPathSelectorExtensionSupport(json: JsValue) {
    def \/(first: String, other: String*): MultiJsLookup = {
      val lookup = json \ first
      MultiJsLookup(json, lookup, other.map(json \ _))
    }
  }

}
