package id.kawalcovid19.api.caseHistory.commons

import scala.concurrent.{ExecutionContext, Future}

trait FuturifySupport {

  implicit class FuturifyExtensionSupport[T](f: => T)(implicit ec: ExecutionContext) {
    def future = {
      val fn = () => f
      Future(fn())
    }
  }
}
