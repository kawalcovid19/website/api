package id.kawalcovid19.api.caseHistory.commons

import scala.util.{Success, Try}

trait TryManySupport {

  implicit class TryManyExtensionSupport(trying: Try.type) {
    def fromMany[T1, T2](raw: T1)(first: T1 => T2, other: (T1 => T2)*): Try[T2] = {
      other
        .foldLeft(Try(first(raw))) { (tried, fun) =>
          tried.fold(_ => Try(fun(raw)), Success(_))
        }
    }
  }

}
