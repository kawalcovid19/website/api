package id.kawalcovid19.api.caseHistory.commons

case class +>[A, B](a: A, b: B)
trait Combinator[P[_]] {
  def combine[A, B](a: P[A], b: P[B]): P[A +> B]
}

object Combinator {
  class CombinatorOf[P[_]: Combinator] {
    def of[A, B](implicit fromA: P[A], fromB: P[B]) = combinator[P, A, B]
  }
  def apply[P[_]: Combinator] = new CombinatorOf[P]
}

trait CombinatorSupport {
  implicit def combinator[P[_], A, B](implicit combinator: Combinator[P], fromA: P[A], fromB: P[B]): P[A +> B] = {
    combinator.combine(fromA, fromB)
  }

}
