package id.kawalcovid19.api.caseHistory.commons

import java.util.concurrent.ThreadLocalRandom
import scala.util.{Random => ScalaRandom}

trait StringUtilSupport {

  implicit class StringSupport(str: String) {
    def leftPad(maxLength: Int = 7) = StringUtil.leftPad(str, maxLength)
    def hashGroup                   = StringUtil.hashGroup(str)
    def toRandomPaddedString = {
      val hashSpace = 10
      val random    = StringUtil.hashGroup(str, hashSpace).toString
      StringUtil.leftPad(random, hashSpace)
    }
  }

  implicit class StringListSupport(list: List[String]) {
    def pickOne = ScalaRandom.shuffle(list).head
  }

}

object StringUtil {

  private val charSequence = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9') :+ '-' toList

  private def nextSequence(len: Int = 20) = {
    val stringBuilder = StringBuilder.newBuilder
    val seqSize       = charSequence.size
    val current       = ThreadLocalRandom.current()
    (0 until len).foreach { _ =>
      val idx = current.nextInt(seqSize)
      stringBuilder + charSequence(idx)
    }
    stringBuilder.toString()
  }

  def leftPad(str: String, maxLength: Int = 7) = {
    "0" * (maxLength - str.length) + str
  }

  def hashGroup(str: String, maximumHashSpace: Int = 200): Long = {
    val charCodeSum = str.toLowerCase.foldLeft(0L) { (memo, char) =>
      memo + char.toInt
    }
    charCodeSum % maximumHashSpace
  }

  def createRandomString: String = createRandomString()

  def createRandomString(size: Int = 10): String = nextSequence(size)

  def createRandomStringList(maxCount: Int = 10) = {
    val current = ThreadLocalRandom.current()
    (0 until current.nextInt(maxCount)).map(_ => createRandomString).toList
  }
}
