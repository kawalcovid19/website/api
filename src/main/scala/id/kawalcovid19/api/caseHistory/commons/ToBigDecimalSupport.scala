package id.kawalcovid19.api.caseHistory.commons

import scala.util.Try

trait ToBigDecimalSupport {

  private val transformer = List(
    (str: String) => BigDecimal(str.toInt),
    (str: String) => BigDecimal(str.toLong),
    (str: String) => BigDecimal(str.toFloat),
    (str: String) => BigDecimal(str.toDouble)
  )

  implicit class StringToBigDecimalExtensionSupport(str: String) {
    def toBigDecimal = {
      Try
        .fromMany(str)(transformer.head, transformer.tail: _*)
    }
  }

  implicit class LongToBigDecimalExtensionSupport(long: Long) {
    def toBigDecimal = BigDecimal(long)
  }

  implicit class DoubleToBigDecimalExtensionSupport(double: Double) {
    def toBigDecimal = BigDecimal(double)
  }

}
