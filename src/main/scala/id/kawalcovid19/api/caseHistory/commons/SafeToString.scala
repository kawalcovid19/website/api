package id.kawalcovid19.api.caseHistory.commons

import java.util.UUID

import org.joda.time.DateTime

trait SafeToString[T] {
  def write(raw: T): String
}

trait SafeStringWrapper {
  def safeToString: String
}

trait SafeToStringImplicitSupport {

  implicit val forString: SafeToString[String]     = _.toString
  implicit val forInt: SafeToString[Int]           = _.toString
  implicit val forLong: SafeToString[Long]         = _.toString
  implicit val forBoolean: SafeToString[Boolean]   = _.toString
  implicit val forDateTime: SafeToString[DateTime] = _.toString()
  implicit def forUUID: SafeToString[UUID]         = _.toString
  implicit def forOption[A: SafeToString] = new SafeToString[Option[A]] {
    override def write(raw: Option[A]) = raw.map(implicitly[SafeToString[A]].write).getOrElse("")
  }

  implicit def forSeq[A: SafeToString] = new SafeToString[Seq[A]] {
    override def write(raw: Seq[A]) = {
      val writer = implicitly[SafeToString[A]]
      raw.map(writer.write).mkString(",")
    }
  }

  implicit def toSafeStringWrapper[A](raw: A)(implicit writer: SafeToString[A]): SafeStringWrapper =
    new SafeStringWrapper {
      override def safeToString = writer.write(raw)
    }

}
