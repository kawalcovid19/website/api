package id.kawalcovid19.api.caseHistory.commons

import scala.util.Try

trait MapReaderSupport {

  private def orElse[L, R, V](current: Either[L, R], resolver: List[() => Either[L, R]]): Either[L, R] = {
    current match {
      case Right(_) => current
      case Left(_) =>
        resolver match {
          case Nil => current
          case head :: tail =>
            orElse(head(), tail)
        }
    }
  }

  trait MapValueReader[From, T] { self =>
    type Ref[T2] = MapValueReader[From, T2]
    def read(raw: From): Either[Throwable, T]
    def map[T2](f: T => Either[Throwable, T2]): MapValueReader[From, T2] = {
      new MapValueReader[From, T2] {
        override def read(raw: From) = {
          for {
            value  <- self.read(raw)
            result <- f(value)
          } yield result
        }
      }
    }

    def tryMap[T2](f: T => T2): MapValueReader[From, T2] = {
      new MapValueReader[From, T2] {
        override def read(raw: From) = {
          for {
            value  <- self.read(raw)
            result <- Try(f(value)).toEither
          } yield result
        }
      }
    }
  }

  object MapValueReader {

    def firstOf[From, T](first: MapValueReader[From, T], others: MapValueReader[From, T]*): MapValueReader[From, T] =
      new MapValueReader[From, T] {
        override def read(raw: From) = {
          orElse(first.read(raw), others.map { reader => () =>
            reader.read(raw)
          }.toList)
        }
      }
  }

  case class ValueLookup[R](opt: Option[R], key: String) {
    def asEitherOpt[T](implicit reader: MapValueReader[R, T]): Either[Throwable, Option[T]] = {
      opt
        .fold(Right(None): Either[Throwable, Option[T]])(
          bs => Right(implicitly[MapValueReader[R, T]].read(bs).toOption)
        )
    }
    def asEither[T](implicit reader: MapValueReader[R, T]): Either[Throwable, T] =
      opt
        .fold(
          Left(new IllegalArgumentException(s"${key} is not exists!")): Either[Throwable, T]
        )(bs => implicitly[MapValueReader[R, T]].read(bs))
    def as[T](implicit reader: MapValueReader[R, T]) = asEither[T].right.get
    def asOrElse[T](orElse: => T)(implicit reader: MapValueReader[R, T]) = {
      asEither[T] match {
        case Left(_)      => orElse
        case Right(value) => value
      }
    }
  }

  case class MultiKeyValueLookup[R](first: ValueLookup[R], lookups: List[ValueLookup[R]]) {
    def asEither[T](implicit reader: MapValueReader[R, T]): Either[Throwable, T] = {
      orElse(first.asEither[T], lookups.map(item => () => item.asEither[T]))
    }
    def as[T](implicit reader: MapValueReader[R, T]) = asEither[T].right.get
    def asOrElse[T](orElse: => T)(implicit reader: MapValueReader[R, T]) = {
      asEither[T] match {
        case Left(_)      => orElse
        case Right(value) => value
      }
    }
  }

  implicit class MapPathLookup[T](map: Map[String, T]) {
    def \(key: String) = ValueLookup(map.get(key), key)
    def firstOf(key: String, others: String*) = {
      MultiKeyValueLookup(\(key), others.map(\).toList)
    }
  }
}
