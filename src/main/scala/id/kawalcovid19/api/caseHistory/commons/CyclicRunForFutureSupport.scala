package id.kawalcovid19.api.caseHistory.commons

import java.util.concurrent.atomic.AtomicBoolean
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import akka.pattern._
import akka.stream.ActorMaterializer

class Cancellable {
  private val ref = new AtomicBoolean(false)
  def isCancelled = ref.get()
  def cancel: Unit = {
    if (!isCancelled) ref.set(true)
  }
}

case class CancellableWithFuture(cancellable: Cancellable, onDone: Future[Unit])

trait CyclicRunForFutureSupport {

  implicit class CyclicRunForFutureExtensionSupport[A](future: => Future[A]) {
    def cyclicRun(implicit materializer: ActorMaterializer, ec: ExecutionContext): CancellableWithFuture = {
      cyclicRunWithTimeout(180.days)
    }

    def cyclicRunWithTimeout(
        timeout: FiniteDuration,
        delay: Option[FiniteDuration] = None,
        onWaiting: Option[String] = None
    )(implicit materializer: ActorMaterializer, ec: ExecutionContext): CancellableWithFuture = {
      val cancellable = new Cancellable
      def cycle(): Future[Unit] = {
        val scheduler = materializer.system.scheduler
        Future
          .firstCompletedOf(
            after(timeout, scheduler)(Future.unit) ::
              (() => future)() ::
              Nil
          )
          .recover {
            case _: Throwable =>
          }
          .flatMap { _ =>
            if (!cancellable.isCancelled) {
              val duration = delay.getOrElse(10.seconds)
              onWaiting.foreach(str => materializer.system.log.info(str))
              after(duration, scheduler)(cycle())
            } else Future.unit
          }
      }
      val onDone = cycle()
      CancellableWithFuture(cancellable, onDone)
    }
  }

}
