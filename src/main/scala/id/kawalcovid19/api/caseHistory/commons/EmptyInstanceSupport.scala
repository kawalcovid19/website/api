package id.kawalcovid19.api.caseHistory.commons

trait EmptyProvider[T] {
  def empty: T
}

trait EmptyInstanceSupport {
  def Empty[T: EmptyProvider] = implicitly[EmptyProvider[T]].empty
}
