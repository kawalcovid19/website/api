package id.kawalcovid19.api.caseHistory.commons

import scala.util.Try

trait MapUtilitySupport {

  trait AnyTo {
    val a: Any
    def as[T]             = a.asInstanceOf[T]
    override def toString = a.toString
    def toInt             = as[Int]
    def toLong            = as[Long]
    def toBoolean         = as[Boolean]
  }

  trait OptionAnyTo {
    val a: Option[Any]
    val or: () => Any
    def as[T] = a.getOrElse(or()).asInstanceOf[T]
  }

  implicit class MapExtension(map: => Map[String, Any]) {
    def *(times: Int) = (0 until times).map(_ => map).toList
    def id = map.get("id").flatMap { anyId =>
      Try(anyId.asInstanceOf[Long]).toOption
    }
    def value(name: String) = new AnyTo {
      override val a: Any = map(name)
    }
    def valueOrElse(name: String, orElse: => Any) = new OptionAnyTo {
      override val or: () => Any  = () => orElse
      override val a: Option[Any] = map.get(name)
    }
    def foldValue[A](name: String)(ifEmpty: => A)(f: Any => A) = {
      map.get(name).fold(ifEmpty)(f)
    }
  }
}
