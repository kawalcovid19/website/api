package id.kawalcovid19.api.caseHistory.commons

import java.util.concurrent.ThreadLocalRandom
import scala.concurrent.duration._
import scala.util.Try

object Random {

  private val charSequence = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9') :+ '-' toList

  private def nextSequence(len: Int = 20) = {
    val stringBuilder = StringBuilder.newBuilder
    val seqSize       = charSequence.size
    val current       = ThreadLocalRandom.current()
    (0 until len).foreach { _ =>
      val idx = current.nextInt(seqSize)
      stringBuilder + charSequence(idx)
    }
    stringBuilder.toString()
  }

  def randomString = { nextSequence(10) }

  def randomWithNegative(seed: Int): Double = {
    val double = ThreadLocalRandom.current().nextDouble()
    double * seed * (if (System.currentTimeMillis() % 2 == 0) 1 else -1)
  }

  def backoffDuration(
      minBackoff: FiniteDuration,
      maxBackoff: FiniteDuration,
      restartCount: Double,
      randomFactor: Double
  ) = {
    val rnd = 1.0 + ThreadLocalRandom.current().nextDouble() * randomFactor
    Try(maxBackoff.min(minBackoff * math.pow(2, restartCount)) * rnd).getOrElse(maxBackoff) match {
      case f: FiniteDuration => f.toSeconds.seconds
      case _                 => maxBackoff
    }
  }

  case class BackoffWithRestartCount(minBackoff: FiniteDuration, maxBackoff: FiniteDuration, randomFactor: Double) {
    private var _restartCount = 0
    def restartCount          = _restartCount
    def incrementRestartCount = _restartCount + 1
    def resetRestartCount     = _restartCount = 0
    def decrementRestartCount = {
      _restartCount = _restartCount - 1
      if (_restartCount < 0) resetRestartCount
    }
    def backoffDuration = Random.backoffDuration(minBackoff, maxBackoff, restartCount, randomFactor)
  }
}
