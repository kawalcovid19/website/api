package id.kawalcovid19.api.caseHistory.commons

import play.api.libs.json.Json

class Tokenized(list: List[String]) {
  def words = list.map(str => str.replaceAll("[^a-zA-Z0-9_#]", ""))
  def asMapCount = {
    val map = list.foldLeft(Map.empty[String, Long]) { (memo, str) =>
      val realStr = str.replaceAll("[^a-zA-Z0-9_#,;]", "")
      memo.get(realStr) match {
        case Some(count) => memo + (realStr -> (count + 1))
        case _           => memo + (realStr -> 1)
      }
    }
    map
  }

  def asHashTagCount = asPrefixedCount("#")

  def asPrefixedCount(prefix: String) = {
    val map = list.foldLeft(Map.empty[String, Long]) {
      case (memo, str) if str.startsWith(prefix) =>
        memo.get(str) match {
          case Some(count) => memo + (str -> (count + 1))
          case _           => memo + (str -> 1)
        }
      case (memo, _) => memo
    }
    Json.toJson(map)
  }
}

object Tokenized {
  def apply(str: String): Tokenized = {
    new Tokenized(str.toLowerCase.split("\\s").toList)
  }
}
