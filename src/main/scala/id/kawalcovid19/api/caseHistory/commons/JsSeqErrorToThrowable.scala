package id.kawalcovid19.api.caseHistory.commons

import play.api.libs.json.{JsError, JsPath, JsResult, JsonValidationError}

trait JsSeqErrorToThrowable {

  implicit class EitherSeqToError[I](either: Either[Seq[(JsPath, Seq[JsonValidationError])], I]) {
    def errify = either match {
      case Right(value) => Right(value)
      case Left(value)  => Left(JsResult.Exception(JsError(value)))
    }
  }

}
