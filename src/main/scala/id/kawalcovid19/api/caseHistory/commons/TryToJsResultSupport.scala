package id.kawalcovid19.api.caseHistory.commons

import scala.util.Try

import play.api.libs.json.{JsError, JsResult, JsSuccess}

trait TryToJsResultSupport {
  implicit class TryToJsResult[T](tried: Try[T]) {
    def toJsResult: JsResult[T] = tried.fold(e => JsError(e.getMessage), r => JsSuccess(r))
  }

  implicit class SomethingToJsResult[T](something: => T) {
    def asResult = Try(something).fold(e => JsError(e.getMessage), r => JsSuccess(r))
  }
}
