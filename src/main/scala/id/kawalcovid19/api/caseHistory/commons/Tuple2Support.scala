package id.kawalcovid19.api.caseHistory.commons

case class ->[A, B](_1: A, _2: B) extends Product2[A, B]
object Tuple {
  def unapply[A, B](arg: A -> B): Option[(A, B)] = Some(arg._1 -> arg._2)
}

trait Tuple2Support {
  implicit def arrowToTuple2[A, B](arrow: A -> B): (A, B)  = arrow._1 -> arrow._2
  implicit def tuple2ToArrow[A, B](tuple2: (A, B)): A -> B = ->(tuple2._1, tuple2._2)
  implicit class ArrowSupport[A](a: A) {
    def ~>[B](b: B): A -> B = ->(a, b)
  }
}
