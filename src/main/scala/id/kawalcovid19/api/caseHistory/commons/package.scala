package id.kawalcovid19.api.caseHistory

package object commons
    extends TryManySupport
    with ToBigDecimalSupport
    with TryToJsResultSupport
    with MapReaderSupport
    with FuturifySupport
    with IgnoreAndRunAkkaStreamSourceSupport
    with EmptyInstanceSupport
    with AppenderSupport
    with Tuple2Support
    with TrySupport
    with PrintFlowStageSupport
    with PlayJsonMultiPathSelectorSupport
    with CombinatorSupport
    with DateTimeSupport
    with MapUtilitySupport
    with EitherToSupport
    with SafeToStringImplicitSupport
    with JsSeqErrorToThrowable
    with CombinatorInstanceSupport
    with CyclicRunForFutureSupport
    with FutureEitherToFailedSupport
