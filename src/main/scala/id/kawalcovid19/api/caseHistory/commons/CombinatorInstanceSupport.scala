package id.kawalcovid19.api.caseHistory.commons

import anorm.{RowParser, ~}

trait CombinatorInstanceSupport {

  implicit val combinatorForRowParser: Combinator[RowParser] = new Combinator[RowParser] {
    override def combine[A, B](a: RowParser[A], b: RowParser[B]): RowParser[A +> B] = a ~ b map {
      case a ~ b => +>(a, b)
    }
  }
}
