package id.kawalcovid19.api.caseHistory.commons

import scala.util.Try

trait TrySupport {

  implicit class TryExtensionSupport[A](f: => A) {
    def getOrElse[B <: A](orElse: => B): A = Try(f).getOrElse(orElse)
  }

}
