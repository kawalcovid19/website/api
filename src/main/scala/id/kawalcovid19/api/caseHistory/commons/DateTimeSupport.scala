package id.kawalcovid19.api.caseHistory.commons

import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

import akka.http.scaladsl.unmarshalling.Unmarshaller
import io.circe.{Encoder, Json => CirceJson}
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatterBuilder, ISODateTimeFormat}
import org.rogach.scallop.{DefaultConverters, ValueConverter}
import play.api.libs.json._

object DateTimeParser {
  private val dateTimeFormatList = List(
    "dd/MM/yy HH.mm",
    "dd/MM/yy HH:mm",
    "dd/MM/yyyy HH.mm",
    "dd/MM/yyyy HH:mm",
    "EEE MMM dd HH:mm:ss Z yyyy",
    "yyyy-MM-dd'T'HH:mm:ssZ",
    "YYYY-MM-dd'T'HH:mm:ssZZZZ",
    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
    "yyyy-MM-d'T'HH",
    "yyyy-MM-d HH:mm:ss",
    "yyyy-MM-dd HH:mm:ss",
    "yyyy-MM-dd HH",
    "yyyy-MM-dd",
    "dd/MM/yy",
    "yyyy-MM-dd/HH",
    "d MMM",
    "dd MMM"
  )
  private val jodaParser = ISODateTimeFormat.dateTime().getParser :: dateTimeFormatList.map(
    DateTimeFormat.forPattern(_).getParser
  )
  private val formatter  = new DateTimeFormatterBuilder().append(null, jodaParser.toArray).toFormatter
  def parse(str: String) = formatter.parseDateTime(str)
}

trait DateTimeSupport extends DefaultConverters {
  private val oneHour = 1.hour toMillis

  implicit val ordering: Ordering[DateTime] = new Ordering[DateTime] {
    override def compare(x: DateTime, y: DateTime) = {
      if (x.getMillis < y.getMillis) -1
      else if (x.getMillis > y.getDayOfMonth) 1
      else 0
    }
  }

  implicit val dateTimeArgsScallop: ValueConverter[DateTime] = stringConverter.flatMap { str =>
    Try(DateTimeParser.parse(str)) match {
      case Success(dt) => Right(Some(dt))
      case Failure(e)  => Left(e.getMessage)
    }
  }

  implicit val dateTimeEntityUnMarshaller: Unmarshaller[String, DateTime] =
    Unmarshaller.strict[String, DateTime](DateTimeParser.parse)

  implicit val dateTimeOrdering: Ordering[DateTime] = new Ordering[DateTime] {
    override def compare(x: DateTime, y: DateTime) = x compareTo y
  }

  implicit val dateTimeCirceEncoder: Encoder[DateTime] = new Encoder[DateTime] {
    override def apply(a: DateTime) = CirceJson.fromString(a.toString("yyyy-MM-dd'T'HH:mm:ssZZ"))
  }

  implicit val dateTimeJsonParser: Format[DateTime] = new Format[DateTime] {
    override def writes(o: DateTime): JsValue = JsString(o.toString("yyyy-MM-dd'T'HH:mm:ssZZ"))
    override def reads(json: JsValue): JsResult[DateTime] = {
      Try {
        List(
          json.asOpt[String].map { str =>
            DateTimeParser.parse(str)
          },
          json.asOpt[Long].map { ts =>
            new DateTime(ts)
          }
        ).collectFirst {
          case Some(d) => d
        }.head
      }.fold(e => JsError(e.getMessage), result => JsSuccess(result))
    }
  }

  implicit class DateTimeUtilSupport(dt: DateTime) {
    def toLowerHour                           = dt.minus(dt.getMillis % oneHour)
    def toLowerHour(interval: FiniteDuration) = dt.minus(dt.getMillis % interval.toMillis)
    def toUpperHour = {
      val addendum = oneHour - (dt.getMillis % oneHour)
      if (addendum == 0) dt
      else dt.plus(addendum)
    }
    def to(other: DateTime)    = DateRange(dt, other)
    def until(other: DateTime) = DateRange(dt, other.minusMillis(1))
    def >(other: DateTime)     = ordering.gt(dt, other)
    def <(other: DateTime)     = ordering.lt(dt, other)
  }

  implicit class DateTimePairUtilSupport(tuple: (DateTime, DateTime)) {
    def interval = tuple._2.minus(tuple._1.getMillis)
  }

}

object DateTimeSupport extends DateTimeSupport {
  def toLowerHour(dt: DateTime)          = dt.toLowerHour
  def toUpperHour(dt: DateTime)          = dt.toUpperHour
  def to(dt: DateTime, dst: DateTime)    = dt to dst
  def until(dt: DateTime, dst: DateTime) = dt until dst
}

case class DatePair(start: DateTime, end: DateTime, max: FiniteDuration = 24.hour) {

  def isWithinEnd(other: DateTime) = {
    (start.isBefore(other) && end.plusHours(2).isAfter(other)) || start.isEqual(other)
  }

  def isMaxed(other: DateTime) = {
    end.minus(start.getMillis).getMillis >= max.toMillis
  }

  def rangeInMillis = end.minus(start.getMillis).getMillis

  def rangeInHour = rangeInMillis / 1000 / 60 / 60

  def copyEnd(other: DateTime) = {
    if (other.isAfter(end)) copy(end = other) else this
  }

  override def toString: String =
    s"DatePair(${start.toString("yyyy-MM-dd/HH")} - ${end.toString("yyyy-MM-dd/HH")}, ${rangeInHour} hour span)"
}

object DatePair {
  def apply(start: DateTime): DatePair = DatePair(start, start.plusHours(1))
}

case class DatePairAccumulator(current: DatePair, accumulated: List[DatePair]) {
  def append(other: DateTime) = {
    val isWithinCurrent = current.isWithinEnd(other)
    val isMaxed         = current.isMaxed(other)
    if (isWithinCurrent && !isMaxed) {
      copy(current = current.copyEnd(other))
    } else {
      copy(current = DatePair(other), accumulated = current :: accumulated)
    }
  }
  def finalizePair = current :: accumulated
}

object DatePairAccumulator {
  def apply(start: DateTime): DatePairAccumulator = DatePairAccumulator(DatePair(start), List.empty)
}

sealed case class DateRange(
    start: DateTime,
    end: DateTime,
    step: FiniteDuration = DateRange.defaultStep
) extends Iterable[DateTime] {
  def by(other: FiniteDuration) = copy(step = other)

  override def iterator: Iterator[DateTime] = {
    if (end.isBefore(start)) DateRange.decrementRange(end, start, step)
    else DateRange.incrementRange(start, end, step)
  }
}

object DateRange {
  val defaultStep = 1.hour

  private[DateRange] def decrementRange(start: DateTime, end: DateTime, step: FiniteDuration) = {
    new Iterator[DateTime] {
      private var current = end
      override def next(): DateTime = {
        val ret = current
        current = current.minus(step.toMillis)
        ret
      }
      override def hasNext: Boolean = current.getMillis >= start.getMillis
    }
  }

  private[DateRange] def incrementRange(start: DateTime, end: DateTime, step: FiniteDuration) = {
    new Iterator[DateTime] {
      private var current = start
      override def next(): DateTime = {
        val ret = current
        current = current.plus(step.toMillis)
        ret
      }
      override def hasNext: Boolean = current.getMillis <= end.getMillis
    }
  }

}
