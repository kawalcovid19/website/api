package id.kawalcovid19.api.caseHistory.commons

import scala.concurrent.{ExecutionContext, Future}

trait FutureEitherToFailedSupport {

  implicit class FutureEitherToFailedExtensionSupport[A](f: Future[Either[Throwable, A]]) {
    def errify(implicit ec: ExecutionContext): Future[A] = {
      for {
        either <- f
        result <- either match {
          case Right(value) => Future.successful(value)
          case Left(value)  => Future.failed(value)
        }
      } yield result
    }
  }

}
