package id.kawalcovid19.api.caseHistory.pipelines

import scala.concurrent.ExecutionContext

import akka.NotUsed
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source

import id.kawalcovid19.api.caseHistory.models.SpreadSheet
import id.kawalcovid19.api.caseHistory.parsers.{FlowRenderer, Parser}
import id.kawalcovid19.api.server.auth.{GoogleApiKey, GoogleServiceAccount}

object RetrieveAndParse {

  def createSource[T: Parser](
      poolQueue: SuperHttpPoolQueue,
      spreadSheet: SpreadSheet,
      apiKey: GoogleApiKey,
      serviceAccountOpt: Option[GoogleServiceAccount] = None
  )(implicit materializer: ActorMaterializer, ec: ExecutionContext): Source[Either[Throwable, T], NotUsed] = {
    val log = materializer.system.log
    Source
      .single()
      .mapAsync(1) { _ =>
        serviceAccountOpt
          .map { serviceAccount =>
            RetrieveFromGoogleSheet.retrieveUsingServiceCredentials(poolQueue, spreadSheet, serviceAccount)
          }
          .getOrElse {
            RetrieveFromGoogleSheet.retrieve(poolQueue, spreadSheet, apiKey)
          }
          .map { json =>
            Right(json)
          }
          .recover {
            case e: Throwable =>
              log.error(e, "Error on retrieving from google!")
              Left(e)
          }
      }
      .collect { case Right(json) => json }
      .mapConcat(raw => {
        val either = (raw \ "values")
          .validate[List[List[String]]]
          .asEither
        if (either.isLeft) {

          log.warning(
            "[#RetrieveAndParse] Error on parsing {} from google sheet {} with error {}",
            raw,
            spreadSheet,
            either.left.get
          )
        }
        either.getOrElse(List.empty)
      })
      .via(FlowRenderer.render[T](spreadSheet.headerPosition))
  }

}
