package id.kawalcovid19.api.caseHistory.pipelines

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success}
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.{ActorAttributes, ActorMaterializer, OverflowStrategy, QueueOfferResult, Supervision}
import akka.stream.scaladsl.{Keep, Sink, Source}

// https://doc.akka.io/docs/akka-http/current/implications-of-streaming-http-entity.html#integrating-with-akka-streams
// https://doc.akka.io//docs/akka-http/current/client-side/host-level.html?language=scala#using-the-host-level-api-with-a-queue

class SuperHttpPoolQueue(queueSize: Int)(
    implicit system: ActorSystem,
    materializer: ActorMaterializer,
    ec: ExecutionContext
) {
  private val decider: Supervision.Decider = { e: Throwable =>
    system.log.warning("[#SuperHttpPoolQueueWithCrawlerProxy] Receiving error on queue {}", e)
    Supervision.Resume
  }
  private val (queue, onDone) = Source
    .queue[(HttpRequest, SuperHttpPoolQueue.ResponseContext)](queueSize, OverflowStrategy.dropNew)
    .via(Http().superPool[SuperHttpPoolQueue.ResponseContext]())
    .mapAsyncUnordered(queueSize * 2) {
      case (Success(resp), context) =>
        context.promise
          .completeWith(Unmarshal(resp).to(context.unmarshaller, ec, materializer))
          .future
      case (Failure(err), context) => context.promise.failure(err).future
    }
    .withAttributes(ActorAttributes.supervisionStrategy(decider))
    .toMat(Sink.ignore)(Keep.both)
    .run()

  def enqueue[T](request: HttpRequest)(implicit ec: ExecutionContext, um: Unmarshaller[HttpResponse, T]): Future[T] = {
    val responseContext = SuperHttpPoolQueue.ResponseContext()
    queue.offer(request -> responseContext).flatMap {
      case QueueOfferResult.Enqueued       => responseContext.promise.future
      case QueueOfferResult.Dropped        => Future.failed(new RuntimeException("Queue overflowed."))
      case QueueOfferResult.Failure(cause) => Future.failed(cause)
      case QueueOfferResult.QueueClosed =>
        Future.failed(new RuntimeException("Queue is closed (pool shut down) while running the request."))
    }
  }
}

object SuperHttpPoolQueue {
  private[SuperHttpPoolQueue] trait ResponseContext {
    type T
    val promise: Promise[T]
    val unmarshaller: Unmarshaller[HttpResponse, T]
  }

  object ResponseContext {

    type Aux[M] = ResponseContext { type T = M }

    implicit def apply[M]()(implicit _um: Unmarshaller[HttpResponse, M]): ResponseContext.Aux[M] = new ResponseContext {
      override type T = M
      override val promise      = Promise[M]
      override val unmarshaller = _um
    }
  }

}
