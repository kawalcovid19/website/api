package id.kawalcovid19.api.caseHistory.pipelines

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import id.nolimit.sqlLayer.Sql.{ParameterMap, SqlLayer}
import id.nolimit.sqlLayer._
import id.nolimit.sqlLayer.connections._

object IngestToDb {

  def flow[T: TableName](onDuplicateKeyUpdate: Option[String] = None)(
      implicit materializer: ActorMaterializer,
      ec: ExecutionContext,
      pool: ConnectionPool,
      toInsertMap: T => ParameterMap
  ) = {
    val log = materializer.system.log
    Flow[T]
      .groupedWithin(100, 10.seconds)
      .mapAsync(3) { seq =>
        Future {
          log.info(s"Begin saving ${seq.size} to db!")
          val insert = SqlLayer
            .insert[T](
              seq.map { element =>
                toInsertMap(element)
              }.toList
            )
          val result = onDuplicateKeyUpdate
            .map(str => insert.onDuplicate(str))
            .getOrElse(insert.run)
          if (result.isFailure) log.error(result.failed.get, "Error on ingestion!")
          result
        }.recover {
          case e: Throwable =>
            log.error(e, "Error on inserting patient!")
        }
      }
  }
}
