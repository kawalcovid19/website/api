package id.kawalcovid19.api.caseHistory.pipelines

import scala.concurrent.{ExecutionContext, Future}

import id.nolimit.sqlLayer.Sql.SqlLayer
import id.nolimit.sqlLayer._
import id.nolimit.sqlLayer.connections.ConnectionPool

import id.kawalcovid19.api.caseHistory.commons._
import id.kawalcovid19.api.caseHistory.models.{CaseSummary, Patient, PatientInformationSummary}

object ShowSummary {
  import primaryKey.support.long._
  def retrieveLatestPatients(implicit pool: ConnectionPool, ec: ExecutionContext) = {

    Future {
      val query =
        s"""
           |SELECT patients.*
           |FROM patients
           |INNER JOIN (
           |  SELECT case_id, MAX(created_at) AS updated_at
           |  FROM patients GROUP BY case_id
           |) AS max_per_group ON patients.case_id = max_per_group.case_id AND patients.created_at = max_per_group.updated_at
           |""".stripMargin

      SqlLayer
        .read[WithId[Patient]](query)
    }
  }

  def calculateSummary(implicit pool: ConnectionPool, ec: ExecutionContext) = {
    retrieveLatestPatients
      .map(_.foldLeft(Empty[PatientInformationSummary])(_ append _.f2))
  }

  def retrieveLatestCaseSummary(implicit pool: ConnectionPool, ec: ExecutionContext) = {
    Future {
      val query =
        s"""
           |SELECT * FROM patient_summaries
           |ORDER BY timestamp DESC
           |LIMIT 0, 1
           |""".stripMargin
      SqlLayer
        .read[CaseSummary](query)
        .headOption
        .getOrElse(Empty[CaseSummary])
    }
  }

}
