package id.kawalcovid19.api.caseHistory.pipelines

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink, Source}
import id.nolimit.sqlLayer.Sql.ParameterMap
import id.nolimit.sqlLayer.TableName
import id.nolimit.sqlLayer.connections.ConnectionPool
import org.joda.time.DateTime

import id.kawalcovid19.api.caseHistory.models.SpreadSheet
import id.kawalcovid19.api.caseHistory.parsers.Parser
import id.kawalcovid19.api.server.auth.{GoogleApiKey, GoogleServiceAccount}

object Runner {

  def runIngestionPipeline[T: TableName: Parser](
      poolQueue: SuperHttpPoolQueue,
      spreadSheet: SpreadSheet,
      onDuplicateKeyUpdate: Option[String] = None,
      apiKey: GoogleApiKey,
      serviceAccountOpt: Option[GoogleServiceAccount]
  )(
      implicit materializer: ActorMaterializer,
      ec: ExecutionContext,
      pool: ConnectionPool,
      toInsertMap: T => ParameterMap
  ) = {
    val log = materializer.system.log
    Source
      .tick(5.seconds, 10.minutes, ())
      .flatMapConcat { _ =>
        log.info(s"Retrieving from ${spreadSheet} at ${DateTime.now()}")
        RetrieveAndParse
          .createSource[T](poolQueue, spreadSheet, apiKey, serviceAccountOpt)
      }
      .map { either =>
        if (either.isLeft) log.info(s"Receiving error from parsing! ${either}")
        either
      }
      .collect { case Right(value) => value }
      .via(IngestToDb.flow[T](onDuplicateKeyUpdate))
      .toMat(Sink.ignore)(Keep.right)
      .run()
  }

}
