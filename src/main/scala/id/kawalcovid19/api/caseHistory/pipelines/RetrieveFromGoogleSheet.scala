package id.kawalcovid19.api.caseHistory.pipelines

import java.io.ByteArrayInputStream
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import akka.http.scaladsl.settings.{ClientConnectionSettings, ConnectionPoolSettings}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.google.auth.oauth2.GoogleCredentials
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import play.api.libs.json.JsValue

import id.kawalcovid19.api.caseHistory.models.SpreadSheet
import id.kawalcovid19.api.server.auth.{GoogleApiKey, GoogleServiceAccount}

object RetrieveFromGoogleSheet extends PlayJsonSupport {

  def retrieveUsingServiceCredentials(
      poolQueue: SuperHttpPoolQueue,
      spreadSheet: SpreadSheet,
      serviceAccount: GoogleServiceAccount
  )(implicit materializer: ActorMaterializer, ec: ExecutionContext) = {

    for {
      token <- Future {
        val is = new ByteArrayInputStream(ByteString(serviceAccount.credentials).toArray)
        GoogleCredentials
          .fromStream(is)
          .createScoped("https://www.googleapis.com/auth/spreadsheets.readonly")
          .refreshAccessToken()
      }
      result <- {
        import akka.http.scaladsl.model.Uri.Path
        val path = Path./("v4") / "spreadsheets" / spreadSheet.sheetId / "values" / spreadSheet.range.formatted
        val query = Uri.Query(
          "access_token"   -> token.getTokenValue,
          "majorDimension" -> "ROWS"
        )

        val uri = Uri("https://sheets.googleapis.com/").withPath(path).withQuery(query)
        val req = HttpRequest(uri = uri, method = HttpMethods.GET)
        poolQueue.enqueue[JsValue](req)
      }
    } yield result
  }

  def retrieve(
      poolQueue: SuperHttpPoolQueue,
      spreadSheet: SpreadSheet,
      apiKey: GoogleApiKey
  )(implicit materializer: ActorMaterializer, ec: ExecutionContext) = {
    import akka.http.scaladsl.model.Uri.Path

    val path = Path./("v4") / "spreadsheets" / spreadSheet.sheetId / "values" / spreadSheet.range.formatted
    val query = Uri.Query(
      "key"            -> apiKey.value,
      "majorDimension" -> "ROWS"
    )
    val uri = Uri("https://sheets.googleapis.com/").withPath(path).withQuery(query)
    val req = HttpRequest(uri = uri, method = HttpMethods.GET)
    poolQueue.enqueue[JsValue](req)
  }

}
