package id.kawalcovid19.api.caseHistory

import javax.ws.rs.{GET, Path}
import scala.concurrent.ExecutionContext

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import id.nolimit.sqlLayer.connections.ConnectionPool
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse

import id.kawalcovid19.api.caseHistory.models.PatientSummaryResponse
import id.kawalcovid19.api.caseHistory.pipelines.ShowSummary

class CaseHistoryRoute(
    patientInfoSummaryRepo: ShowSummary.type
)(implicit ec: ExecutionContext, pool: ConnectionPool)
    extends FailFastCirceSupport {

  final val route: Route = summary

  @Path("api/case/summary")
  @GET
  @Operation(
    summary = "API case summary endpoint",
    description = "API case summary provides latest case number of COVID-19 in Indonesia",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Service status response",
        content = Array(
          new Content(
            mediaType = "application/json",
            schema = new Schema(implementation = classOf[PatientSummaryResponse])
          )
        )
      )
    )
  )
  def summary: Route =
    path("case" / "summary") {
      onSuccess(patientInfoSummaryRepo.retrieveLatestCaseSummary) { result =>
        complete(PatientSummaryResponse(result.confirmed, result.recovered, result.deceased, result.createdAt))
      }
    }

}
