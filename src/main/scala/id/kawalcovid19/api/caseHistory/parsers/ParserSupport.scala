package id.kawalcovid19.api.caseHistory.parsers

import org.joda.time.DateTime

import id.kawalcovid19.api.caseHistory.commons._

trait ParserSupport {

  type MapStringValueReader[T] = MapValueReader[String, T]

  implicit val stringParser: MapStringValueReader[String] = new MapStringValueReader[String] {
    override def read(raw: String) = Right(raw.trim)
  }

  implicit val longParser: MapStringValueReader[Long]         = stringParser.tryMap(_.toLong)
  implicit val intParser: MapStringValueReader[Int]           = stringParser.tryMap(_.toInt)
  implicit val dateTimeParser: MapStringValueReader[DateTime] = stringParser.tryMap(DateTimeParser.parse(_))
}
