package id.kawalcovid19.api.caseHistory.parsers

trait Parser[T] {
  def read(map: Map[String, String]): Either[Throwable, T]
}

object Parser {
  def parse[T: Parser](map: Map[String, String]) = implicitly[Parser[T]].read(map)
}
