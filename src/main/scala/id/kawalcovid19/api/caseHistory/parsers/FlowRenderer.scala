package id.kawalcovid19.api.caseHistory.parsers

import akka.stream.scaladsl.{Flow, Source}

object FlowRenderer {

  def render[T: Parser](headerPosition: Int) = {
    val oneBasedIndex = headerPosition + 1
    Flow[List[String]]
      .prefixAndTail(oneBasedIndex)
      .flatMapConcat {
        case (seq, source) =>
          if (seq.size < oneBasedIndex) {
            Source.empty
          } else {
            val header = seq.last.map(_.toLowerCase)
            source.map { body =>
              header
                .zip(body)
                .toMap
            }
          }

      }
      .map(Parser.parse[T](_))
  }

}
