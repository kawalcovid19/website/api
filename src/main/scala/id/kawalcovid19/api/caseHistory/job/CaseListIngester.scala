package id.kawalcovid19.api.caseHistory.job

import scala.concurrent.ExecutionContext
import akka.actor.{Actor, ActorLogging, Props}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink}
import id.nolimit.sqlLayer.connections.ConnectionPool
import id.kawalcovid19.api.caseHistory.models.{Patient, SheetRange, SpreadSheet}
import id.kawalcovid19.api.caseHistory.pipelines.{IngestToDb, RetrieveAndParse, SuperHttpPoolQueue}
import id.kawalcovid19.api.server.auth.GoogleApiKey

class CaseListIngester(
    poolQueue: SuperHttpPoolQueue,
    apiKey: GoogleApiKey,
    _pool: ConnectionPool
) extends Actor
    with ActorLogging {
  import CaseListIngester._

  implicit val mat: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext   = this.context.system.dispatcher
  implicit val pool: ConnectionPool   = _pool

  private val caseListSheet =
    SpreadSheet("1ma1T9hWbec1pXlwZ89WakRk-OfVUQZsOCFl4FwZxzVw", SheetRange("Daftar Kasus", "A1", "U200"), 1)

  private val tapErrors: PartialFunction[Result[Patient], Result[Patient]] = {
    case res @ Left(err) =>
      log.warning("[#StartCaseListIngest] Error parsing record: {}", err.getMessage)
      res
    case res @ Right(value) =>
      log.debug("[#StartCaseListIngest] Record: {}", value)
      res
  }

  private val successOnly: PartialFunction[Result[Patient], Patient] = {
    case Right(value) => value
  }

  override def receive: Receive = {
    case StartCaseListIngest =>
      log.info("[#StartCaseListIngest] Start case list ingestion")
      RetrieveAndParse
        .createSource[Patient](poolQueue, caseListSheet, apiKey)
        .map(tapErrors)
        .collect(successOnly)
        .via(IngestToDb.flow[Patient]())
        .toMat(Sink.ignore)(Keep.right)
        .run()
  }
}

object CaseListIngester {
  def props(poolQueue: SuperHttpPoolQueue, apiKey: GoogleApiKey, pool: ConnectionPool): Props =
    Props(new CaseListIngester(poolQueue, apiKey, pool))

  case object StartCaseListIngest
}
