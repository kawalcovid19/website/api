package id.kawalcovid19.api.caseHistory.job

import scala.concurrent.ExecutionContext
import akka.actor.{Actor, ActorLogging, Props}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink}
import id.nolimit.sqlLayer.connections.ConnectionPool
import id.kawalcovid19.api.caseHistory.models.{CaseSummary, SheetRange, SpreadSheet}
import id.kawalcovid19.api.caseHistory.pipelines.{IngestToDb, RetrieveAndParse, SuperHttpPoolQueue}
import id.kawalcovid19.api.server.auth.{GoogleApiKey, GoogleServiceAccount}

class CaseSummaryIngester(
    poolQueue: SuperHttpPoolQueue,
    apiKey: GoogleApiKey,
    serviceAccount: Option[GoogleServiceAccount],
    _pool: ConnectionPool
) extends Actor
    with ActorLogging {
  import CaseSummaryIngester._

  implicit val mat: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext   = this.context.system.dispatcher
  implicit val pool: ConnectionPool   = _pool

  private val caseSummarySheet =
    SpreadSheet("1McDKkM-BO8Evbhr7iAjraCoCT5V44d-8oHUZpDpUuh0", SheetRange("Sheet1", "A1", "E100"), 0)

  private val onDuplicateKeyUpdate =
    s"""confirmed = VALUES(confirmed),
       |recovered = VALUES(recovered),
       |deceased = VALUES(deceased),
       |created_at = VALUES(created_at)
       |""".stripMargin

  private val tapErrors: PartialFunction[Result[CaseSummary], Result[CaseSummary]] = {
    case res @ Left(err) =>
      log.warning("[#StartCaseSummaryIngest] Error parsing record: {}", err.getMessage)
      res
    case res @ Right(value) =>
      log.debug("[#StartCaseSummaryIngest] Record: {}", value)
      res
  }

  private val successOnly: PartialFunction[Result[CaseSummary], CaseSummary] = {
    case Right(value) => value
  }

  override def receive: Receive = {
    case StartCaseSummaryIngest =>
      log.info("[#StartCaseSummaryIngest] Start case summary ingestion")
      RetrieveAndParse
        .createSource[CaseSummary](poolQueue, caseSummarySheet, apiKey, serviceAccount)
        .map(tapErrors)
        .collect(successOnly)
        .via(IngestToDb.flow[CaseSummary](Some(onDuplicateKeyUpdate)))
        .toMat(Sink.ignore)(Keep.right)
        .run()
  }
}

object CaseSummaryIngester {
  def props(
      poolQueue: SuperHttpPoolQueue,
      apiKey: GoogleApiKey,
      serviceAccount: Option[GoogleServiceAccount],
      pool: ConnectionPool
  ): Props =
    Props(new CaseSummaryIngester(poolQueue, apiKey, serviceAccount, pool))

  case object StartCaseSummaryIngest
}
