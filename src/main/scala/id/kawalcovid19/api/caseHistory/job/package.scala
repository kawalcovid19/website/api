package id.kawalcovid19.api.caseHistory

package object job {
  type Result[T] = Either[Throwable, T]
}
