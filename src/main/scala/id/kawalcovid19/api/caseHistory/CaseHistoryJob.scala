package id.kawalcovid19.api.caseHistory

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import id.nolimit.sqlLayer.connections.ConnectionPool
import id.kawalcovid19.api.caseHistory.job.{CaseListIngester, CaseSummaryIngester}
import id.kawalcovid19.api.caseHistory.pipelines.SuperHttpPoolQueue
import id.kawalcovid19.api.server.auth.{GoogleApiKey, GoogleServiceAccount}
import id.kawalcovid19.api.server.config.GoogleConfig

/** Supervisor Actor for Case history Google Sheet ingestion.
  *
  * @param config Google Docs / Sheet configuration.
  * @param pool Database connection pool.
  */
class CaseHistoryJob(
    poolQueue: SuperHttpPoolQueue,
    config: GoogleConfig,
    pool: ConnectionPool
) extends Actor
    with ActorLogging {
  import CaseHistoryJob._

  private val googleApiKey         = GoogleApiKey(config.apiKey)
  private val googleServiceAccount = GoogleServiceAccount.fromConfig(config)

  val caseListIngester: ActorRef =
    this.context.actorOf(CaseListIngester.props(poolQueue, googleApiKey, pool))

  val caseSummaryIngester: ActorRef =
    this.context.actorOf(CaseSummaryIngester.props(poolQueue, googleApiKey, googleServiceAccount, pool))

  override def receive: Receive = {
    case StartIngest =>
      log.info("[#StartIngest] Start case history ingestion")
      caseListIngester ! CaseListIngester.StartCaseListIngest
      caseSummaryIngester ! CaseSummaryIngester.StartCaseSummaryIngest
  }
}

object CaseHistoryJob {
  def props(poolQueue: SuperHttpPoolQueue, config: GoogleConfig, pool: ConnectionPool): Props =
    Props(new CaseHistoryJob(poolQueue, config, pool))

  case object StartIngest
}
