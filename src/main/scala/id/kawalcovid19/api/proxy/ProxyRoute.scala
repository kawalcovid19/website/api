package id.kawalcovid19.api.proxy

import scala.concurrent.ExecutionContext

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport

class ProxyRoute(
    proxyService: ProxyService
)(implicit ec: ExecutionContext)
    extends PlayJsonSupport {

  final val route: Route =
    pathPrefix("proxy") {
      get {
        path(Segment / Remaining) { (key, path) =>
          parameterMap { paramMap =>
            extractRequest { request =>
              onSuccess(proxyService.getRequest(key, path, paramMap, request.headers)) { response =>
                complete(response)
              }
            }
          }
        }
      }
    }
}
