package id.kawalcovid19.api.proxy

import scala.collection.immutable.{Seq => ISeq}
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpEntity.CloseDelimited
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{
  ContentType,
  ContentTypes,
  HttpHeader,
  HttpResponse,
  ResponseEntity,
  StatusCode,
  StatusCodes
}
import akka.stream.scaladsl.Source
import akka.util.ByteString
import scalacache.caffeine.CaffeineCache
import sttp.client._
import sttp.client.akkahttp.AkkaHttpBackend
import sttp.model.{Header, Uri}

trait ProxyService {
  def getRequest(key: String, path: String, paramMap: Map[String, String], headers: Seq[HttpHeader])(
      implicit ec: ExecutionContext
  ): Future[HttpResponse]
}

object ProxyService {

  sealed trait ProxyServiceResponseCase          extends Throwable
  case object NoProxyEntryCase                   extends ProxyServiceResponseCase
  case class ProxyEntryCase(value: HttpResponse) extends ProxyServiceResponseCase
}

class ProxyServiceSttpImplementation(proxyMap: Map[String, String], ttl: Option[FiniteDuration])(
    implicit system: ActorSystem
) extends ProxyService {
  import ProxyService._

  private implicit val backend = AkkaHttpBackend.usingActorSystem(system)
  private implicit val cache   = CaffeineCache[HttpResponse]

  private final val log = system.log

  private final val IGNORED_REQUEST_HEADERS = Set(
    "Host",          // Unable to process response if specified(?)
    "Timeout-Access" // akka-http-internal concerns
  )

  /** These headers are reserved within akka-http internals */
  private final val IGNORED_RESPONSE_HEADERS = Set(
    "Content-Type",
    "Content-Length",
    "Date",
    "Server"
  )

  private final val NOT_FOUND_RESPONSE =
    HttpResponse(status = StatusCodes.NotFound)

  /** Proxied GET request.
    *
    * @param key Proxy host key.
    * @param path Relative path to proxied resource.
    * @param paramMap Query parameters as Map.
    * @param headers List of request headers.
    * @param ec Execution Context for task scheduling.
    * @return Async, memoized, HTTP response if successful.
    */
  def getRequest(key: String, path: String, paramMap: Map[String, String], headers: Seq[HttpHeader])(
      implicit ec: ExecutionContext
  ): Future[HttpResponse] =
    (for {
      host <- proxyHostOrNotFound(key)
      uri  <- cacheOrUri(host, path, paramMap)
      headerMap = toMap(headers)
      response <- sendGetRequest(uri, headerMap)
    } yield response).recoverWith {
      case NoProxyEntryCase =>
        Future.successful(NOT_FOUND_RESPONSE)
      case ProxyEntryCase(response) =>
        Future.successful(response)
    }

  /** Get the proxy host given its config key or return not found.
    *
    * @param key Proxy host config key.
    * @return Proxy host entry if found.
    */
  private[this] def proxyHostOrNotFound(key: String): Future[String] = {
    log.debug(s"[proxyHostOrNotFound] Check proxy map host for entry '${key}'")

    proxyMap
      .get(key.toLowerCase)
      .fold {
        log.debug(s"[proxyHostOrNotFound] No host for entry '${key}'")

        Future.failed[String](NoProxyEntryCase)
      } { host =>
        log.debug(s"[proxyHostOrNotFound] Found host '${host}'")

        Future.successful(host)
      }
  }

  /** Get the URI or short-circuit to a proxy entry if found.
    *
    * @param host Proxied resource host.
    * @param path Proxied resource relative path.
    * @param paramMap Query parameters as Map.
    * @return URI if no cache entry, or cache entry if found.
    */
  private[this] def cacheOrUri(host: String, path: String, paramMap: Map[String, String]): Future[Uri] = {
    import scalacache._
    import scalacache.modes.sync._

    val uri = toUri(host, path, paramMap)

    log.debug(s"[cacheOrUri] Check cache entry for URI '${uri.toString()}'")

    sync
      .get(uri.toString())
      .fold {
        log.debug(s"[cacheOrUri] Found existing cache entry for URI '${uri.toString()}'")

        Future.successful(uri)
      } { entry =>
        log.debug(s"[cacheOrUri] No cache entry for URI '${uri.toString()}'")

        Future.failed[Uri](ProxyEntryCase(entry))
      }
  }

  /** Send GET request to the proxied resource.
    *
    * @param uri Proxied resource URI.
    * @param headerMap Request headers as Map.
    * @param ec Execution Context for task scheduling.
    * @return Async, memoized, HTTP response if successful.
    */
  private[this] def sendGetRequest(uri: Uri, headerMap: Map[String, String])(
      implicit ec: ExecutionContext
  ): Future[HttpResponse] =
    for {
      _ <- Future.successful(log.info(s"[sendGetRequest] Proxying GET request for '${uri.toString()}'"))
      req = sttp.client.basicRequest.headers(headerMap).get(uri)
      res <- req.send()
    } yield res match {
      case Response(Right(body), code, _, headers, _) =>
        log.info("[sendGetRequest] Caching successful response")
        memoize(uri) {
          HttpResponse(
            status = StatusCode.int2StatusCode(code.code),
            entity = toEntity(headers, body),
            headers = ISeq(toHttpHeaders(headers): _*)
          )
        }
      case Response(Left(body), _, _, headers, _) =>
        log.error("[sendGetRequest] Unable to parse response")
        HttpResponse(
          status = StatusCodes.BadGateway,
          entity = toEntity(headers, body),
          headers = ISeq(toHttpHeaders(headers): _*)
        )
      case Response(_, _, _, headers, _) =>
        log.error("[sendGetRequest] Unexpected error while proxying request")
        HttpResponse(
          status = StatusCodes.InternalServerError,
          headers = ISeq(toHttpHeaders(headers): _*)
        )
    }

  /** Memoize HTTP response based on the URI as its key.
    *
    * @param uri URI of proxied resource.
    * @param result HTTP response to cache.
    * @return HTTP response.
    */
  private[this] def memoize(uri: Uri)(result: HttpResponse): HttpResponse = {
    import scalacache._
    import scalacache.modes.sync._

    sync.put(uri.toString())(result, ttl)

    result
  }

  /** Combine host and path to absolute path as (sttp) URI.
    *
    * @param host Host to proxy to.
    * @param path Relative path to resource.
    * @return Absolute path to proxy to as URI.
    */
  private[this] def toUri(host: String, path: String, paramMap: Map[String, String]): Uri =
    if (paramMap.isEmpty) uri"${host.stripSuffix("/")}/${path.stripPrefix("/")}"
    else uri"${host.stripSuffix("/")}/${path.stripPrefix("/")}?${paramMap}"

  /** Convert list of headers from request context into a Map, filtering out `Host` and `Timeout-Access` (which is
    * akka-http internal concerns.
    *
    * @param headers List of HTTP headers from source.
    * @return HTTP header Map, minus `Timeout-Access`.
    */
  private[this] def toMap(headers: Seq[HttpHeader]): Map[String, String] = {
    log.debug("[toMap] Convert (akka-http) HTTP headers to header key-value / map")
    headers
      .sortBy(_.lowercaseName())
      .foldLeft(Map.empty[String, String]) {
        case (acc, header) if IGNORED_REQUEST_HEADERS.exists(_.equalsIgnoreCase(header.name())) =>
          acc
        case (acc, header) =>
          acc + (header.name() -> header.value())
      }
  }

  /** Convert response body to Response Entity, preserving its content type.
    *
    * @param headers List of HTTP headers from destination.
    * @param body Response body as string.
    * @return akka-http Response Entity.
    */
  private[this] def toEntity(headers: Seq[Header], body: String): ResponseEntity = {
    log.debug(s"[toEntity] Convert (sttp) response body to (akka-http) response entity")

    val sourceBody = Source.single(ByteString(body.stripLineEnd))
    val contentType =
      headers
        .find(_.name.equalsIgnoreCase("Content-Type"))
        .map(header => ContentType.parse(header.value))

    contentType match {
      case Some(Right(ct)) =>
        log.debug(s"[toEntity] Convert entity with content type '${ct.toString()}'")
        CloseDelimited(ct, sourceBody)
      case _ =>
        log.warning("[toEntity] No content type in response")
        CloseDelimited(ContentTypes.NoContentType, sourceBody)
    }
  }

  /** Convert list of headers from response context, filtering out header keys which will be overridden later ann
    * appending it with `X-Source-*` prefix.
    *
    * @param headers List of HTTP headers from destination.
    * @return HTTP headers, with reserved ones for use by akka-http prefixed.
    */
  private[this] def toHttpHeaders(headers: Seq[Header]): Seq[HttpHeader] = {
    log.debug("[toHttpHeaders] Convert (sttp) HTTP headers to (akka-htto) headers")

    headers.foldLeft(Seq.empty[HttpHeader]) {
      case (acc, header) if IGNORED_RESPONSE_HEADERS.exists(_.equalsIgnoreCase(header.name)) =>
        acc :+ RawHeader(s"X-Source-${header.name}", header.value)
      case (acc, header) =>
        acc :+ RawHeader(header.name, header.value)
    }
  }
}
