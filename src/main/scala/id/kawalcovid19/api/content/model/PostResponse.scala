package id.kawalcovid19.api.content.model

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

final case class PostResponse(
    id: Int,
    title: String,
    author: User,
    content: String,
    excerpt: String,
    slug: String,
    date: String,
    date_gmt: String,
    modified: Option[String],
    modified_gmt: Option[String],
    categories: Seq[Category],
    tags: Seq[Tag]
)

object PostResponse {
  implicit val encoder: Encoder[PostResponse] = deriveEncoder[PostResponse]
}
