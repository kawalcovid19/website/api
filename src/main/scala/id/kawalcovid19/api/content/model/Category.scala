package id.kawalcovid19.api.content.model

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

final case class Category(
    id: Int,
    count: Int,
    description: String,
    name: String,
    slug: String
)

object Category {
  implicit val encoder: Encoder[Category] = deriveEncoder[Category]
}
