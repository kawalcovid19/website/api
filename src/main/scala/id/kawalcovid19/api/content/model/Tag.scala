package id.kawalcovid19.api.content.model

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

final case class Tag(
    id: Int,
    count: Int,
    description: String,
    name: String,
    slug: String
)

object Tag {
  implicit val encoder: Encoder[Tag] = deriveEncoder[Tag]
}
