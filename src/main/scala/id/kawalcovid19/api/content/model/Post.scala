package id.kawalcovid19.api.content.model

final case class Post(
    id: Int,
    title: RenderedString,
    author: Int,
    content: RenderedString,
    excerpt: RenderedString,
    slug: String,
    date: String,
    date_gmt: String,
    modified: Option[String],
    modified_gmt: Option[String],
    categories: Seq[Int],
    tags: Seq[Int]
) {
  def toPostResponse(author: User, categories: List[Category], tags: List[Tag]): PostResponse = {
    PostResponse(
      this.id,
      this.title.rendered,
      author,
      this.content.rendered,
      this.excerpt.rendered,
      this.slug,
      this.date,
      this.date_gmt,
      this.modified,
      this.modified_gmt,
      categories,
      tags
    )
  }
}

final case class RenderedString(rendered: String)
