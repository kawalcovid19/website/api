package id.kawalcovid19.api.content.model

import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

final case class User(
    id: Int,
    name: String,
    description: String,
    slug: String,
    avatar_urls: Map[String, String]
)

object User {
  implicit val encoder: Encoder[User] = deriveEncoder[User]
}
