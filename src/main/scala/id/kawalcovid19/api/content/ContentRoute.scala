package id.kawalcovid19.api.content

import javax.ws.rs.{GET, Path}

import scala.concurrent.ExecutionContext
import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import io.circe.syntax._
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import id.kawalcovid19.api.content.model.{Category, PostResponse}

class ContentRoute(contentService: ContentService)(
    implicit _actor: ActorSystem,
    _mat: ActorMaterializer,
    _ec: ExecutionContext
) {

  final val route: Route = pathPrefix("content") { posts ~ post ~ faq ~ about ~ categories }

  @Path("api/content/posts")
  @GET
  @Operation(
    summary = "Content for WordPress posts endpoint",
    description = "Content endpoint to fetch (with any query) WordPress posts. It will returning all the matched posts " +
      "with the query and sorted by latest created post.",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Service status response",
        content = Array(
          new Content(
            mediaType = "application/json",
            schema = new Schema(implementation = classOf[List[PostResponse]])
          )
        )
      )
    )
  )
  def posts: Route =
    path("posts") {
      parameter("author".as[Int].?, "categories".?, "tags".?, "limit".as[Int].?, "offset".as[Int].?) {
        (author, categories, tags, limit, offset) =>
          val categorySet = categories.map(_.split(",").map(_.toInt).toSet).getOrElse(Set.empty[Int])
          val tagSet      = tags.map(_.split(",").map(_.toInt).toSet).getOrElse(Set.empty[Int])
          onSuccess(contentService.getPosts(author, categorySet, tagSet, limit, offset)) { result =>
            complete(result.asJson.noSpaces)
          }
      }
    }

  @Path("api/content/post/:id")
  @GET
  @Operation(
    summary = "Content for WordPress single post endpoint",
    description = "Content endpoint to fetch single WordPress post by Id.",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Service status response",
        content = Array(
          new Content(
            mediaType = "application/json",
            schema = new Schema(implementation = classOf[PostResponse])
          )
        )
      )
    )
  )
  def post: Route =
    path("post" / Remaining) { id =>
      onSuccess(contentService.getPost(id.toInt)) { result =>
        complete(result.asJson.noSpaces)
      }
    }

  @Path("api/content/faq")
  @GET
  @Operation(
    summary = "Content for fetching FAQ page",
    description = "Content for fetching FAQ page.",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Service status response",
        content = Array(
          new Content(
            mediaType = "application/json",
            schema = new Schema(implementation = classOf[PostResponse])
          )
        )
      )
    )
  )
  def faq: Route =
    path("faq") {
      onSuccess(contentService.getFaq) { result =>
        complete(result.asJson.noSpaces)
      }
    }

  @Path("api/content/about")
  @GET
  @Operation(
    summary = "Content for fetching About page",
    description = "Content for fetching About page.",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Service status response",
        content = Array(
          new Content(
            mediaType = "application/json",
            schema = new Schema(implementation = classOf[PostResponse])
          )
        )
      )
    )
  )
  def about: Route =
    path("about") {
      onSuccess(contentService.getAbout) { result =>
        complete(result.asJson.noSpaces)
      }
    }

  @Path("api/content/categories")
  @GET
  @Operation(
    summary = "Content for WordPress categories endpoint",
    description = "Content endpoint to fetch WordPress categories. It will returning all categories",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Service status response",
        content = Array(
          new Content(
            mediaType = "application/json",
            schema = new Schema(implementation = classOf[List[Category]])
          )
        )
      )
    )
  )
  def categories: Route =
    path("categories") {
      onSuccess(contentService.getCategories) { result =>
        complete(result.asJson.noSpaces)
      }
    }

}
