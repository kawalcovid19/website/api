package id.kawalcovid19.api.content

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import akka.http.scaladsl.settings.{ClientConnectionSettings, ConnectionPoolSettings}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import io.circe.Decoder
import scalacache.caffeine.CaffeineCache

trait HttpRequestClient {

  def get[A: Decoder](url: String): Future[A]

}

class HttpRequestClientImpl(implicit _actor: ActorSystem, _mat: ActorMaterializer, _ec: ExecutionContext)
    extends HttpRequestClient {

  private implicit val caffeineCache = CaffeineCache[String]

  private lazy val log = _mat.system.log

  private lazy val settings = ClientConnectionSettings(_mat.system)
    .withIdleTimeout(5.minutes)
    .withConnectingTimeout(5.minutes)

  private lazy val connectionSettings = ConnectionPoolSettings(_mat.system)
    .withConnectionSettings(settings)

  private val ttl = Some(1.hours)

  def get[A: Decoder](url: String): Future[A] = {
    import scalacache._
    import scalacache.modes.scalaFuture._
    val resultString = cachingF(url)(ttl)(sendRequest(url))
    parse(resultString)
  }

  private[content] def sendRequest(url: String): Future[String] = {
    val req = HttpRequest(uri = Uri(url), method = HttpMethods.GET)
    Http().singleRequest(req, settings = connectionSettings).flatMap(Unmarshal(_).to[String])
  }

  private def parse[A: Decoder](resF: Future[String]): Future[A] = {
    import io.circe.parser.decode

    resF.map { res =>
      decode[A](res) match {
        case Right(result) => result
        case Left(err) =>
          log.info(s"Receiving error from parsing! $err")
          throw new Exception("Receiving error from input parsing!")
      }
    }
  }

}
