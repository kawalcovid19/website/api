package id.kawalcovid19.api.content

import scala.concurrent.{ExecutionContext, Future}

import id.kawalcovid19.api.content.model._
import id.kawalcovid19.api.server.config.ContentConfig
import io.circe.generic.auto._

trait ContentService {

  def getPosts(
      author: Option[Int],
      categories: Set[Int],
      tags: Set[Int],
      limit: Option[Int],
      offset: Option[Int]
  ): Future[List[PostResponse]]

  def getPost(id: Int): Future[Option[PostResponse]]

  def getFaq: Future[PostResponse]

  def getAbout: Future[PostResponse]

  def getCategories: Future[List[Category]]

}

class ContentServiceImpl(httpClient: HttpRequestClient, contentConfig: ContentConfig)(implicit ec: ExecutionContext)
    extends ContentService {

  private[content] val faqId   = 2
  private[content] val aboutId = 282

  def getPosts(
      author: Option[Int] = None,
      categories: Set[Int] = Set.empty,
      tags: Set[Int] = Set.empty,
      limit: Option[Int] = None,
      offset: Option[Int] = None
  ): Future[List[PostResponse]] = {
    val postsF      = httpClient.get[List[Post]](contentConfig.postsUrl)
    val categoriesF = httpClient.get[List[Category]](contentConfig.categoryUrl)
    val tagsF       = httpClient.get[List[Tag]](contentConfig.tagsUrl)
    val usersF      = httpClient.get[List[User]](contentConfig.usersUrl)

    for {
      postList    <- postsF
      categoryMap <- categoriesF.map(_.map(cat => cat.id -> cat).toMap)
      tagMap      <- tagsF.map(_.map(tag => tag.id -> tag).toMap)
      userMap     <- usersF.map(_.map(user => user.id -> user).toMap)
    } yield {
      val defaultUser = User(-1, "Unknown", "", "", Map.empty)
      val from        = offset.getOrElse(0)
      val until       = from + limit.getOrElse(contentConfig.defaultPostsResponse)
      val filteredPosts = postList
        .filter { post =>
          (author.isEmpty || author.contains(post.author)) &&
          (categories.isEmpty || post.categories.exists(categories.contains)) &&
          (tags.isEmpty || post.tags.exists(tags.contains))
        }
        .slice(from, until)
      filteredPosts.map { post =>
        val author     = userMap.getOrElse(post.author, defaultUser)
        val categories = post.categories.flatMap(catId => categoryMap.get(catId)).toList
        val tag        = post.tags.flatMap(tagId => tagMap.get(tagId)).toList
        post.toPostResponse(author, categories, tag)
      }
    }
  }

  def getPost(id: Int): Future[Option[PostResponse]] = getPosts(limit = Some(Integer.MAX_VALUE)).map(_.find(_.id == id))

  def getFaq: Future[PostResponse] = getPost(faqId).map(_.getOrElse(throw new Exception("FAQ cannot be found")))

  def getAbout: Future[PostResponse] = getPost(aboutId).map(_.getOrElse(throw new Exception("`About` cannot be found")))

  def getCategories: Future[List[Category]] = httpClient.get[List[Category]](contentConfig.categoryUrl)

}
