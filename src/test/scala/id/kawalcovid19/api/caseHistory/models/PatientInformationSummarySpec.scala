package id.kawalcovid19.api.caseHistory.models

import org.joda.time.DateTime
import org.scalatest.OptionValues
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class PatientInformationSummarySpec extends AnyWordSpec with Matchers with OptionValues {

  "Patient Information Summary" when {

    "appending Patient data" should {
      val testId = "TEST"
      val now    = DateTime.now

      "retain confirmed count if Patient is not declared positive" in {
        val summary = PatientInformationSummary.EMPTY
        val patient = mockPatient(testId)

        val result = summary.append(patient)

        result.confirmed mustEqual 0
      }

      "append confirmed count if Patient is declared positive" in {
        val summary = PatientInformationSummary.EMPTY
        val patient = mockPatient(testId).copy(declaredPositiveAt = Some(now))

        val result = summary.append(patient)

        result.confirmed mustEqual 1
      }

      "retain recovered count if Patient is not declared recovered" in {
        val summary = PatientInformationSummary.EMPTY
        val patient = mockPatient(testId)

        val result = summary.append(patient)

        result.recovered mustEqual 0
      }

      "append recovered count if Patient is declared recovered" in {
        val summary = PatientInformationSummary.EMPTY
        val patient = mockPatient(testId).copy(recoveredAt = Some(now))

        val result = summary.append(patient)

        result.recovered mustEqual 1
      }

      "retain deceased count if Patient is not declared deceased" in {
        val summary = PatientInformationSummary.EMPTY
        val patient = mockPatient(testId)

        val result = summary.append(patient)

        result.deceased mustEqual 0
      }

      "append deceased count if Patient is declared deceased" in {
        val summary = PatientInformationSummary.EMPTY
        val patient = mockPatient(testId).copy(deceasedAt = Some(now))

        val result = summary.append(patient)

        result.deceased mustEqual 1
      }

      "update with Patient record created timestamp if current one does not exists" in {
        val summary = PatientInformationSummary.EMPTY
        val patient = mockPatient(testId, now)

        val result = summary.append(patient)

        result.lastUpdated.value mustEqual now
      }

      "update with Patient record created timestamp if current one is later than the Patient record" in {
        val summaryDate = now.plusMillis(10)
        val summary     = PatientInformationSummary.EMPTY.copy(lastUpdated = Some(summaryDate))
        val patient     = mockPatient(testId, now)

        val result = summary.append(patient)

        result.lastUpdated.value mustEqual now
      }

      "retain current timestamp if Patient record created timestamp is later than the current record" in {
        val summaryDate = now.minusMillis(10)
        val summary     = PatientInformationSummary.EMPTY.copy(lastUpdated = Some(summaryDate))
        val patient     = mockPatient(testId, now)

        val result = summary.append(patient)

        result.lastUpdated.value mustEqual summaryDate
      }

      "add nationalities total if current record is empty" in {
        val summary     = PatientInformationSummary.EMPTY
        val nationality = "id"
        val patient     = mockPatient(testId).copy(nationality = Some(nationality))

        val result = summary.append(patient)

        result.nationalityMap.size mustEqual 1
        result.nationalityMap.keys must contain(nationality)
        result.nationalityMap(nationality) mustEqual 1L
      }

      "update nationalities total if nationality already exists in the record" in {
        val existing    = Map("id" -> 1L)
        val summary     = PatientInformationSummary.EMPTY.copy(nationalityMap = existing)
        val nationality = "id"
        val patient     = mockPatient(testId).copy(nationality = Some(nationality))

        val result = summary.append(patient)

        result.nationalityMap.size mustEqual 1
        result.nationalityMap.keys must contain(nationality)
        result.nationalityMap(nationality) mustEqual 2L
      }

      "update nationalities if different nationality already exists in the record" in {
        val existing    = Map("us" -> 1L)
        val summary     = PatientInformationSummary.EMPTY.copy(nationalityMap = existing)
        val nationality = "id"
        val patient     = mockPatient(testId).copy(nationality = Some(nationality))

        val result = summary.append(patient)

        result.nationalityMap.size mustEqual 2
        result.nationalityMap.keys must (contain(nationality) and contain("us"))
        result.nationalityMap(nationality) mustEqual 1L
        result.nationalityMap("us") mustEqual 1L
      }

      "add provinces total if current record is empty" in {
        val summary  = PatientInformationSummary.EMPTY
        val province = "dki jakarta"
        val patient  = mockPatient(testId).copy(province = Some(province))

        val result = summary.append(patient)

        result.provinceMap.size mustEqual 1
        result.provinceMap.keys must contain(province)
        result.provinceMap(province) mustEqual 1L
      }

      "update provinces total if province already exists in the record" in {
        val existing = Map("dki jakarta" -> 1L)
        val summary  = PatientInformationSummary.EMPTY.copy(provinceMap = existing)
        val province = "dki jakarta"
        val patient  = mockPatient(testId).copy(province = Some(province))

        val result = summary.append(patient)

        result.provinceMap.size mustEqual 1
        result.provinceMap.keys must contain(province)
        result.provinceMap(province) mustEqual 2L
      }

      "update provinces if different province already exists in the record" in {
        val existing = Map("jawa barat" -> 1L)
        val summary  = PatientInformationSummary.EMPTY.copy(provinceMap = existing)
        val province = "dki jakarta"
        val patient  = mockPatient(testId).copy(province = Some(province))

        val result = summary.append(patient)

        result.provinceMap.size mustEqual 2
        result.provinceMap.keys must (contain(province) and contain("jawa barat"))
        result.provinceMap(province) mustEqual 1L
        result.provinceMap("jawa barat") mustEqual 1L
      }

      "add cluster total if current record is empty" in {
        val summary = PatientInformationSummary.EMPTY
        val cluster = "diamond princess"
        val patient = mockPatient(testId).copy(cluster = Some(cluster))

        val result = summary.append(patient)

        result.clusterMap.size mustEqual 1
        result.clusterMap.keys must contain(cluster)
        result.clusterMap(cluster) mustEqual 1L
      }

      "update cluster total if cluster already exists in the record" in {
        val existing = Map("diamond princess" -> 1L)
        val summary  = PatientInformationSummary.EMPTY.copy(clusterMap = existing)
        val cluster  = "diamond princess"
        val patient  = mockPatient(testId).copy(cluster = Some(cluster))

        val result = summary.append(patient)

        result.clusterMap.size mustEqual 1
        result.clusterMap.keys must contain(cluster)
        result.clusterMap(cluster) mustEqual 2L
      }

      "update cluster if different cluster already exists in the record" in {
        val existing = Map("jakarta" -> 1L)
        val summary  = PatientInformationSummary.EMPTY.copy(clusterMap = existing)
        val cluster  = "diamond princess"
        val patient  = mockPatient(testId).copy(cluster = Some(cluster))

        val result = summary.append(patient)

        result.clusterMap.size mustEqual 2
        result.clusterMap.keys must (contain(cluster) and contain("jakarta"))
        result.clusterMap(cluster) mustEqual 1L
        result.clusterMap("jakarta") mustEqual 1L
      }

      "add gender total if current record is empty" in {
        val summary = PatientInformationSummary.EMPTY
        val gender  = "diamond princess"
        val patient = mockPatient(testId).copy(gender = Some(gender))

        val result = summary.append(patient)

        result.genderMap.size mustEqual 1
        result.genderMap.keys must contain(gender)
        result.genderMap(gender) mustEqual 1L
      }

      "update gender total if gender already exists in the record" in {
        val existing = Map("l" -> 1L)
        val summary  = PatientInformationSummary.EMPTY.copy(genderMap = existing)
        val gender   = "l"
        val patient  = mockPatient(testId).copy(gender = Some(gender))

        val result = summary.append(patient)

        result.genderMap.size mustEqual 1
        result.genderMap.keys must contain(gender)
        result.genderMap(gender) mustEqual 2L
      }

      "update gender if different gender already exists in the record" in {
        val existing = Map("p" -> 1L)
        val summary  = PatientInformationSummary.EMPTY.copy(genderMap = existing)
        val gender   = "l"
        val patient  = mockPatient(testId).copy(gender = Some(gender))

        val result = summary.append(patient)

        result.genderMap.size mustEqual 2
        result.genderMap.keys must (contain(gender) and contain("p"))
        result.genderMap(gender) mustEqual 1L
        result.genderMap("p") mustEqual 1L
      }
    }

    "updating last updated timestamp" should {

      "apply the provided timestamp if current one does not exists" in {
        val current = Option.empty[DateTime]
        val update  = DateTime.now()

        val result = PatientInformationSummary.updateTimestamp(current, update)

        result.value mustEqual update
      }

      "retain the current timestamp if provided one is after the current one" in {
        val now     = DateTime.now()
        val current = Some(now)
        val update  = now.plusMillis(10)

        val result = PatientInformationSummary.updateTimestamp(current, update)

        result.value mustEqual now
      }

      "update with the provided one if it is before the current one" in {
        val now     = DateTime.now()
        val current = Some(now)
        val update  = now.minusMillis(10)

        val result = PatientInformationSummary.updateTimestamp(current, update)

        result.value mustEqual update
      }
    }

    "appending map count from data" should {
      val emptyMap = Map.empty[String, Long]

      "return empty map for empty map given no data to append" in {
        val data = Option.empty[String]
        val map  = emptyMap

        val result = PatientInformationSummary.appendOptionToMap(data, map)

        result mustEqual Map.empty[String, Long]
      }

      "return map with one entry for empty map given data to append" in {
        val key  = "Test Data"
        val data = Some(key)
        val map  = emptyMap

        val result = PatientInformationSummary.appendOptionToMap(data, map)

        result.size mustBe 1
        result.keys must contain(key.toLowerCase)
        result(key.toLowerCase) mustBe 1L
      }

      "return map with one entry with count incremented for map with existing entry given data to append" in {
        val key  = "Test Data"
        val data = Some(key)
        val map  = PatientInformationSummary.appendOptionToMap(data, emptyMap)

        val result = PatientInformationSummary.appendOptionToMap(data, map)

        result.size mustBe 1
        result.keys must contain(key.toLowerCase)
        result(key.toLowerCase) mustBe 2L
      }

      "return map with one entry with count incremented for map with existing entry given data to append with difference casing" in {
        val key1  = "TEST DATA"
        val data1 = Some(key1)
        val key2  = "Test data"
        val data2 = Some(key2)
        val map   = PatientInformationSummary.appendOptionToMap(data1, emptyMap)

        val result = PatientInformationSummary.appendOptionToMap(data2, map)

        result.size mustBe 1
        result.keys must contain(key1.toLowerCase)
        result(key1.toLowerCase) mustBe 2L
      }

      "return map with two entries for map with different existing entry given data to append" in {
        val key1  = "Test Data 1"
        val data1 = Some(key1)
        val key2  = "Test Data 2"
        val data2 = Some(key2)
        val map   = PatientInformationSummary.appendOptionToMap(data1, emptyMap)

        val result = PatientInformationSummary.appendOptionToMap(data2, map)

        result.size mustBe 2
        result.keys must (contain(key1.toLowerCase) and contain(key2.toLowerCase))
        result(key1.toLowerCase) mustBe 1L
        result(key2.toLowerCase) mustBe 1L
      }
    }
  }

  private[this] def mockPatient(id: String, createdAt: DateTime = DateTime.now): Patient =
    Patient(
      caseId = id,
      age = None,
      gender = None,
      nationality = None,
      province = None,
      city = None,
      firstContactAt = None,
      firstObservedSymptomAt = None,
      firstIsolationAt = None,
      declaredPositiveAt = None,
      recoveredAt = None,
      deceasedAt = None,
      hospitalizedIn = None,
      locationType = None,
      contactReference = None,
      notes = None,
      incubationPeriod = None,
      finalStatus = None,
      createdAt = createdAt,
      cluster = None
    )
}
