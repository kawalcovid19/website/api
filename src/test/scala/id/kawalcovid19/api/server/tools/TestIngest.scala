package id.kawalcovid19.api.server.tools

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink}
import id.kawalcovid19.api.caseHistory.models.{Patient, SheetRange, SpreadSheet}
import id.kawalcovid19.api.caseHistory.pipelines.{RetrieveAndParse, SuperHttpPoolQueue}
import id.kawalcovid19.api.server.auth.{GoogleApiKey, GoogleServiceAccount}
import id.kawalcovid19.api.server.config.AppConfig

object TestIngest {

  def main(args: Array[String]): Unit = {
    implicit val system       = ActorSystem("test-ingest")
    implicit val ec           = system.dispatcher
    implicit val materializer = ActorMaterializer()

    val config               = AppConfig.load
    val googleApiKey         = GoogleApiKey(config.google.apiKey)
    val googleServiceAccount = GoogleServiceAccount.fromConfig(config.google)
    val poolQueue            = new SuperHttpPoolQueue(20)
    val patientSpreadSheet =
      SpreadSheet("1ma1T9hWbec1pXlwZ89WakRk-OfVUQZsOCFl4FwZxzVw", SheetRange("Daftar Kasus", "A1", "U200"), 2)

    RetrieveAndParse
      .createSource[Patient](poolQueue, patientSpreadSheet, googleApiKey, googleServiceAccount)
      .map { result =>
        system.log.info(result.toString)
      }
      .toMat(Sink.ignore)(Keep.right)
      .run()
      .gracefulExit
  }
}
