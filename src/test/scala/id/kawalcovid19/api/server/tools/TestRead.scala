package id.kawalcovid19.api.server.tools

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import id.nolimit.sqlLayer.connections.ConnectionPool

import id.kawalcovid19.api.caseHistory.pipelines.ShowSummary

object TestRead {

  def main(args: Array[String]): Unit = {
    implicit val system       = ActorSystem("test-read")
    implicit val materializer = ActorMaterializer()
    implicit val ec           = system.dispatcher
    implicit val pool         = ConnectionPool.create("main", "mysql")

    ShowSummary.retrieveLatestPatients.map { summary =>
      system.log.info(summary.toString())
    }.gracefulExit
  }
}
