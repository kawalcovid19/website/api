package id.kawalcovid19.api.server.config

import org.scalatest.OptionValues
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.util.Try

class AppConfigSpec extends AnyWordSpec with Matchers with OptionValues {

  "App config" when {

    "reading config" should {

      "able to load application.conf properly" in {
        val tried = Try(AppConfig.load)
        tried.isSuccess mustBe true
      }
    }
  }
}
