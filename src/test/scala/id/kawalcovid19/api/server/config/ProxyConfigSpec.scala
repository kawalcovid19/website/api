package id.kawalcovid19.api.server.config

import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class ProxyConfigSpec extends AnyWordSpec with Matchers {

  "Proxy config" when {

    "parsing routes map" should {

      "return empty map given no route configuration" in {
        val conf     = ProxyConfig(None, None)
        val expected = Map.empty[String, String]

        conf.routesMap mustEqual expected
      }

      "return empty map given empty string route" in {
        val conf     = ProxyConfig(Some(""), None)
        val expected = Map.empty[String, String]

        conf.routesMap mustEqual expected
      }

      "return single entry given one valid string route" in {
        val conf     = ProxyConfig(Some("key=value"), None)
        val expected = Map("key" -> "value")

        conf.routesMap mustEqual expected
      }

      "return all entries given multiple valid string routes" in {
        val conf     = ProxyConfig(Some("key1=value1|key2=value2"), None)
        val expected = Map("key1" -> "value1", "key2" -> "value2")

        conf.routesMap mustEqual expected
      }

      "return keys trimmed given entries with spaces" in {
        val conf     = ProxyConfig(Some("key1=value1| key2=value2"), None)
        val expected = Map("key1" -> "value1", "key2" -> "value2")

        conf.routesMap mustEqual expected
      }

      "return values trimmed given entries with spaces" in {
        val conf     = ProxyConfig(Some("key1=value1 |key2=value2"), None)
        val expected = Map("key1" -> "value1", "key2" -> "value2")

        conf.routesMap mustEqual expected
      }

      "return key and values trimmed given entries with spaces" in {
        val conf     = ProxyConfig(Some("key1=value1 | key2=value2"), None)
        val expected = Map("key1" -> "value1", "key2" -> "value2")

        conf.routesMap mustEqual expected
      }
    }
  }
}
