package id.kawalcovid19.api.content

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import id.kawalcovid19.api.server.config.{ContentConfig, WordPressEndpoint}
import org.mockito.Mockito
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AsyncWordSpec

class ContentServiceSpec extends AsyncWordSpec with Matchers {

  implicit val __actor = Mockito.mock(classOf[ActorSystem])
  implicit val __mat   = Mockito.mock(classOf[ActorMaterializer])

  val contentConfig =
    new ContentConfig("http://www.mock-wp.com", WordPressEndpoint("/posts", "/categories", "/tags", "/users"), 0, 10)
  val httpClientMock = new HttpRequestClientMock(contentConfig)
  val contentService = new ContentServiceImpl(httpClientMock, contentConfig) {
    override val faqId   = 764
    override val aboutId = 745
  }

  "get all posts without any query" in {
    val response = contentService.getPosts(None, Set.empty, Set.empty, None, None)
    response.map { result =>
      result.size mustEqual 3
      result.map(_.id) must contain theSameElementsInOrderAs List(768, 764, 745)
    }
  }

  "get all posts with limit" in {
    val response = contentService.getPosts(None, Set.empty, Set.empty, Some(1), None)
    response.map { result =>
      result.size mustEqual 1
      result.map(_.id) must contain theSameElementsInOrderAs List(768)
    }
  }

  "get all posts with offset" in {
    val response = contentService.getPosts(None, Set.empty, Set.empty, None, Some(1))
    response.map { result =>
      result.size mustEqual 2
      result.map(_.id) must contain theSameElementsInOrderAs List(764, 745)
    }
  }

  "get all posts with author filter" in {
    val response = contentService.getPosts(Some(13), Set.empty, Set.empty, None, None)
    response.map { result =>
      result.size mustEqual 1
      result.map(_.id) must contain theSameElementsInOrderAs List(745)
    }
  }

  "get all posts with categories filter" in {
    val response = contentService.getPosts(None, Set(44), Set.empty, None, None)
    response.map { result =>
      result.size mustEqual 2
      result.map(_.id) must contain theSameElementsInOrderAs List(764, 745)
    }
  }

  "get all posts with tags filter" in {
    val response = contentService.getPosts(None, Set.empty, Set(50), None, None)
    response.map { result =>
      result.size mustEqual 1
      result.map(_.id) must contain theSameElementsInOrderAs List(745)
    }
  }

  "get post with id" in {
    val response = contentService.getPost(768)
    response.map { result =>
      result.map(_.id) mustBe Some(768)
    }
  }

  "cannot find get post with id" in {
    val response = contentService.getPost(999)
    response.map { result =>
      result mustBe None
    }
  }

  "get faq" in {
    val response = contentService.getFaq
    response.map { result =>
      result.id mustBe 764
    }
  }

  "get about" in {
    val response = contentService.getAbout
    response.map { result =>
      result.id mustBe 745
    }
  }

  "get categories" in {
    val response = contentService.getCategories
    response.map { result =>
      result.map(_.id) must contain theSameElementsInOrderAs List(44, 43, 19, 2, 40, 12, 1)
    }
  }

}
