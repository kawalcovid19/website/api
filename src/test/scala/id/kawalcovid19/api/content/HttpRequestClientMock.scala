package id.kawalcovid19.api.content

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import id.kawalcovid19.api.server.config.ContentConfig

import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source

class HttpRequestClientMock(contentConfig: ContentConfig)(
    implicit _actor: ActorSystem,
    _mat: ActorMaterializer,
    _ec: ExecutionContext
) extends HttpRequestClientImpl {

  override def sendRequest(url: String): Future[String] = {
    url match {
      case contentConfig.postsUrl    => Future.successful(getResponse("content/wp-posts.json"))
      case contentConfig.usersUrl    => Future.successful(getResponse("content/wp-users.json"))
      case contentConfig.categoryUrl => Future.successful(getResponse("content/wp-categories.json"))
      case contentConfig.tagsUrl     => Future.successful(getResponse("content/wp-tags.json"))
    }
  }

  private def getResponse(path: String): String = Source.fromResource(path).mkString

}
